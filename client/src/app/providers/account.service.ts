import 'rxjs/Rx';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map'
import {Injectable} from '@angular/core'
import {ApiService} from "./api.service";
import {CurrentUserService} from "./current-user.service";
import {CommonService} from "./common.service";
import {Router} from "@angular/router";

@Injectable()
export class AccountService {

  constructor(public api: ApiService,
              public currentUser: CurrentUserService,
              public router: Router,
              public common: CommonService) {
  }

  resetPassword(email: string): Observable<any> {
    return this.api.post('users/reset', {"email": email})
  }

  logout() {
    console.info("Logged Out");
    localStorage.clear();
    this.currentUser.info = null;
    this.common.pages = this.common.homePages;
    this.router.navigate(['/home']);
    this.common.openSnackBar(`Logged out!`)
  }

  getDetailsViaToken(token) {
    return this.api.get('users/' + token.userId, {access_token: token.id})
  }

  changeAttributes(user) {
    return this.api.put("users/updateUser", {details: user})
  }
}
