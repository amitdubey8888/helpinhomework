import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {CommonService} from "./common.service";
import {ApiService} from "./api.service";

@Injectable()
export class ItemsService {

  public items = [];

  constructor(public common:CommonService,
              public api:ApiService) {
    this.getData();
  }

  query(params?:any) {
    console.log("Items", this.items);
    if (!params) {
      return this.items;
    }

    return this.items.filter((item) => {
      for (let key in params) {
        let field = item[key];
        if (typeof field == 'string' && field.toLowerCase().indexOf(params[key].toLowerCase()) >= 0) {
          return item;
        } else if (field == params[key]) {
          return item;
        }
      }
      return null;
    });
  }

  add(item) {
    this.items.push(item);
  }

  delete(item) {
    this.items.splice(this.items.indexOf(item), 1);
  }

  getData() {
    this.items = [];
    this.api.get(`questions?filter[where][and][0][archived]=false&filter[where][and][1][status]=completed&filter[include]=subject`)
      .map(res => res.json())
      .subscribe(response => {
        this.items = response;
      }, error => {
      });
  }
}
