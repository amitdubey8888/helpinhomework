import {Injectable} from '@angular/core';
import {Http, RequestOptions, URLSearchParams} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class ApiService {
  // baseUrl:string = "http://www.helpinhomework.org/";
  baseUrl: string = "http://35.185.71.191/";
  // baseUrl: string = "http://localhost/";
  url: string = this.baseUrl + "api";  //local

  constructor(public http: Http) {
  }

  get(endpoint: string, params?: any, options?: RequestOptions) {
    if (!options) {
      options = new RequestOptions();
    }

    // Support easy query params for GET requests
    if (params) {
      let p = new URLSearchParams();
      for (let k in params) {
        p.set(k, params[k]);
      }
      // Set the search field if we have params and don't already have
      options.search = !options.search && p || options.search;
    }

    return this.http.get(this.url + '/' + endpoint, options);
  }

  post(endpoint: string, body: any, options?: RequestOptions) {
    return this.http.post(this.url + '/' + endpoint, body, options)
  }

  put(endpoint: string, body: any, options?: RequestOptions) {
    return this.http.put(this.url + '/' + endpoint, body, options);
  }

  delete(endpoint: string, options?: RequestOptions) {
    return this.http.delete(this.url + '/' + endpoint, options);
  }

  patch(endpoint: string, body: any, options?: RequestOptions) {
    return this.http.put(this.url + '/' + endpoint, body, options);
  }

  uploadFile(files, directory?: string) {
    return new Promise((resolve, reject) => {
      let xhr: XMLHttpRequest = new XMLHttpRequest();
      xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      };
      xhr.open('POST', this.baseUrl + 'upload-multiple', true);
      let formData = new FormData();
      formData.append("directory", directory);
      for (let i = 0; i < files.length; i++) {
        formData.append("file", files[i]);
      }
      xhr.send(formData);
    });
  }

  sendMail(message: string, subject?: string, to?: string, email?: string) {
    let details = {
      to: to,
      message: message,
      subject: subject,
      email: email
    };
    return this.http.post(this.url + "/users/sendMail", {details: details})
      .map(res => res.json())
      .subscribe(response => console.log('email res', response), error => console.error('mail error', error));
  }
}
