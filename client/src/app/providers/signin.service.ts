import {Injectable} from '@angular/core';
import {MdDialogRef, MdDialog, MdDialogConfig} from '@angular/material';
import {SigninComponent} from "../components/dialogs/signin/signin.component";
import {Router, UrlSegment} from "@angular/router";

@Injectable()
export class SigninService {

  constructor(private dialog: MdDialog, private router: Router) {
  }

  public showDialog(nextRoute?: UrlSegment[]) {
    let dialogRef: MdDialogRef<SigninComponent>;
    dialogRef = this.dialog.open(SigninComponent, {
      height: '60vh',
      width: '80vw',
      data: {
        nextRoute: nextRoute
      }
    });
    dialogRef.afterClosed()
      .subscribe(nextRoute => {
        let path = ['/home'];
        if (nextRoute && nextRoute.length) {
          path = [nextRoute[0].path]
        }
        this.router.navigate(path)
      });
  }
}
