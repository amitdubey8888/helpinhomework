import {Injectable} from '@angular/core';
import {ApiService} from "./api.service";
import {CommonService} from "./common.service";

@Injectable()
export class DataService {

  constructor(private api: ApiService,
              private common: CommonService) {
  }

  fetchAllQuestions(subjectId?: string) {
    return this.api.get(`questions?filter[where][archived]=false&filter[include]=bids&filter[include]=answers&filter[include]=user&filter[include]=comments&filter[include]=subject${subjectId === 'all' ? '' : '&filter[where][subjectId]='}${subjectId === 'all' ? '' : subjectId}`)
      .map(subjects => subjects.json())
  }

  fetchQuestionById(questionId?: string) {
    return this.api.get(`questions?filter[where][id]=${questionId}&filter[include]=subject&filter[include]=comments&filter[include]=answers&filter[include]=bids&filter[include]=user`)
      .map(res => res.json())
  }

  fetchQuestionsAskedById(userId) {
    return this.api.get(`questions?filter[where][userId]=${userId}&filter[limit]=20&filter[order]=createdAt%20DESC`)
      .map(res => res.json())
  }
}
