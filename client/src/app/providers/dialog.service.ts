import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Rx';
import {MdDialogRef, MdDialog, MdDialogConfig} from '@angular/material';
import {ConfirmComponent} from "../components/dialogs/confirm/confirm.component";

@Injectable()
export class DialogService {

  constructor(private dialog: MdDialog) {
  }

  public open(title: string, message: string): Observable<boolean> {

    let dialogRef: MdDialogRef<ConfirmComponent>;

    dialogRef = this.dialog.open(ConfirmComponent);

    dialogRef.componentInstance.title = title;
    dialogRef.componentInstance.message = message;

    return dialogRef.afterClosed();
  }
}
