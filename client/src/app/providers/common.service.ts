import {Injectable} from '@angular/core';
import {MdSnackBar} from "@angular/material";

@Injectable()
export class CommonService {

  public editQuestionID = '';
  public questionDetails: any = null;
  public pages = [
    {
      "name": "Question Bank",
      "route": "/question-bank"
    },
    {
      "name": "Ask a Question",
      "route": "/ask-a-question"
    },
    {
      "name": "Become a Tutor",
      "route": "/become-a-tutor"
    },
    {
      "name": "Inbox",
      "route": "/login"
    },
    {
      "name": "Upload & Earn Money",
      "route": "/upload-and-earn"
    },
    {
      "name": "Search",
      "route": "/login"
    }
  ];
  public adminPages = [
    {
      "name": "Tutors",
      "route": "/admin/tutors"
    },
    {
      "name": "Students",
      "route": "/admin/students"
    },
    {
      "name": "Questions",
      "route": "/admin/questions"
    },
    {
      "name": "Payments",
      "route": "/admin/payments"
    },
    {
      "name": "Subjects",
      "route": "/admin/subjects"
    }
  ];
  public studentPages = [
    {
      "name": "Question Bank",
      "route": "/question-bank"
    },
    {
      "name": "Ask a Question",
      "route": "/ask-a-question"
    },
    {
      "name": "Inbox",
      "route": "/home"
    },
    {
      "name": "Upload & Earn Money",
      "route": "/upload-and-earn"
    },
    {
      "name": "Search",
      "route": "/home"
    }
  ];
  public tutorPages = [
    {
      "name": "Question Bank",
      "route": "/question-bank"
    },
    {
      "name": "Ask a Question",
      "route": "/ask-a-question"
    },
    {
      "name": "Become a Tutor",
      "route": "/tutor/become-a-tutor"
    },
    {
      "name": "Inbox",
      "route": "/home"
    },
    {
      "name": "Upload & Earn Money",
      "route": "/upload-and-earn"
    },
    {
      "name": "Search",
      "route": "/home"
    }
  ];
  public homePages = [
    {
      "name": "Question Bank",
      "route": "/question-bank"
    },
    {
      "name": "Ask a Question",
      "route": "/ask-a-question"
    },
    {
      "name": "Become a Tutor",
      "route": "/become-a-tutor"
    },
    {
      "name": "Inbox",
      "route": "/login"
    },
    {
      "name": "Upload & Earn Money",
      "route": "/upload-and-earn"
    },
    {
      "name": "Search",
      "route": "/login"
    }
  ];
  public loading: boolean = false;

  constructor(public snackBar: MdSnackBar) {
  }

  openSnackBar(message: string, action?: string) {
    this.snackBar.open(message, action, {
      duration: 5000,
    });
  }

  showProgressBar() {
    setTimeout(() => this.loading = true)
  }

  hideProgressBar() {
    setTimeout(() => this.loading = false)
  }
}
