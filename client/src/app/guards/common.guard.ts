import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {CurrentUserService} from "../providers/current-user.service";
import {CommonService} from "../providers/common.service";
import {SigninService} from "../providers/signin.service";
import {AccountService} from "../providers/account.service";

@Injectable()
export class CommonGuard implements CanActivate {
  constructor(public currentUser: CurrentUserService,
              public common: CommonService,
              public account: AccountService,
              public signin: SigninService) {
  }

  canActivate(next: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (!this.currentUser.info) {
      let token = JSON.parse(localStorage.getItem('tutorToken')) || JSON.parse(localStorage.getItem('studentToken')) || null;
      if (token) {
        this.currentUser.info = token;
        token.realm === 'tutor' ? this.common.pages = this.common.tutorPages : this.common.pages = this.common.studentPages;
        return true;
      }
      else {
        this.signin.showDialog(next.url);
        return false;
      }
    }
    else {
      if (this.currentUser.info.realm == 'student' || this.currentUser.info.realm == 'tutor') {
        this.currentUser.info.realm === 'tutor' ? this.common.pages = this.common.tutorPages : this.common.pages = this.common.studentPages;
        return true
      }
      else {
        this.signin.showDialog(next.url);
        return false
      }
    }
  }
}
