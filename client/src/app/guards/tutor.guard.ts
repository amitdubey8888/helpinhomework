import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {CurrentUserService} from "../providers/current-user.service";
import {CommonService} from "../providers/common.service";
import {SigninService} from "../providers/signin.service";

@Injectable()
export class TutorGuard implements CanActivate {

  constructor(public currentUser: CurrentUserService,
              public common: CommonService,
              public signin: SigninService,
              public router: Router) {
  }

  canActivate(next: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (!this.currentUser.info) {
      let tutorToken = JSON.parse(localStorage.getItem('tutorToken')) || null;
      if (tutorToken) {
        this.currentUser.info = tutorToken;
        this.common.pages = this.common.tutorPages;
        if (next.url[0].path === 'browse-questions') {
          if (this.currentUser.info.status === 'privileged') {
            return true
          }
          else {
            this.router.navigate(['/become-a-tutor']);
            return false
          }
        }
        else {
          return true;
        }
      }
      else {
        this.signin.showDialog(next.url);
        return false;
      }
    }
    else {
      if (this.currentUser.info.realm == 'tutor') {
        this.common.pages = this.common.tutorPages;
        if (next.url[0].path === 'browse-questions') {
          if (this.currentUser.info.status === 'privileged') {
            return true
          }
          else {
            this.router.navigate(['/become-a-tutor']);
            return false
          }
        } else {
          return true
        }
      }
      else {
        this.signin.showDialog(next.url);
        return false
      }
    }
  }
}
