import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {CurrentUserService} from "../providers/current-user.service";
import {CommonService} from "../providers/common.service";
import {SigninService} from "../providers/signin.service";

@Injectable()
export class StudentGuard implements CanActivate {

  constructor(public currentUser: CurrentUserService,
              public common: CommonService,
              public signin: SigninService,
              public router: Router) {
  }

  canActivate(next: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (!this.currentUser.info) {
      let studentToken = JSON.parse(localStorage.getItem('studentToken')) || null;
      if (studentToken) {
        this.currentUser.info = studentToken;
        this.common.pages = this.common.studentPages;
        return true;
      }
      else {
        this.signin.showDialog(next.url);
        return false;
      }
    }
    else {
      if (this.currentUser.info.realm == 'student') {
        this.common.pages = this.common.studentPages;
        return true
      }
      else {
        this.signin.showDialog(next.url);
        return false
      }
    }
  }
}
