import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {CurrentUserService} from "../providers/current-user.service";
import {CommonService} from "../providers/common.service";

@Injectable()
export class AdminGuard implements CanActivate {

  constructor(public currentUser: CurrentUserService,
              public common: CommonService,
              public router: Router) {
  }

  canActivate(next: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (!this.currentUser.info) {
      let adminToken = JSON.parse(localStorage.getItem('adminToken')) || null;
      if (!adminToken) {
        this.router.navigate(['/admin-login']);
        return false;
      }
      else {
        this.common.pages = this.common.adminPages;
        this.currentUser.info = adminToken;
        return true
      }
    }
    else {
      if (this.currentUser.info.realm == 'admin') {
        this.common.pages = this.common.adminPages;
        return true
      }
      else
        return false
    }
  }
}
