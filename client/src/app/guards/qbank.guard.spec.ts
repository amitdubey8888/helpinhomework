import { TestBed, async, inject } from '@angular/core/testing';

import { QbankGuard } from './qbank.guard';

describe('QbankGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [QbankGuard]
    });
  });

  it('should ...', inject([QbankGuard], (guard: QbankGuard) => {
    expect(guard).toBeTruthy();
  }));
});
