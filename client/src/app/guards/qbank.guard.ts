import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {CurrentUserService} from "../providers/current-user.service";

@Injectable()
export class QbankGuard implements CanActivate {

  constructor(public currentUser:CurrentUserService,
              public router:Router) {
  }

  canActivate(next:ActivatedRouteSnapshot,
              state:RouterStateSnapshot):Observable<boolean> | Promise<boolean> | boolean {
    if (!this.currentUser.info) {
      let studentToken = JSON.parse(localStorage.getItem('studentToken')) || null;
      let adminToken = JSON.parse(localStorage.getItem('adminToken')) || null;
      let tutorToken = JSON.parse(localStorage.getItem('tutorToken')) || null;
      this.currentUser.info = studentToken || tutorToken || adminToken;
      return true;
    }
    else
      return true;
  }
}
