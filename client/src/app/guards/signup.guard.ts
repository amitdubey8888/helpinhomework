import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {CurrentUserService} from "../providers/current-user.service";

@Injectable()
export class SignupGuard implements CanActivate {

  constructor(public currentUser:CurrentUserService,
              public router:Router) {
  }

  canActivate(next:ActivatedRouteSnapshot,
              state:RouterStateSnapshot):Observable<boolean> | Promise<boolean> | boolean {
    let adminToken = localStorage.getItem('adminToken') || null;
    let studentToken = localStorage.getItem('studentToken') || null;
    let tutorToken = localStorage.getItem('tutorToken') || null;

    if (!this.currentUser.info) {
      if (adminToken) {
        this.router.navigate(['/admin']);
        return false;
      }
      else if (studentToken) {
        this.router.navigate(['/student']);
        return false;
      }
      else if (tutorToken) {
        this.router.navigate(['/tutor']);
        return false;
      }
      else {
        return true;
      }
    }
    else {
      if (this.currentUser.info.realm == 'admin') {
        this.router.navigate(['/admin']);
        return false;
      }
      else if (this.currentUser.info.realm == 'student') {
        this.router.navigate(['/student']);
        return false;
      }
      else if (this.currentUser.info.realm == 'tutor') {
        this.router.navigate(['/tutor']);
        return false;
      }
      else {
        return true;
      }
    }
  }
}
