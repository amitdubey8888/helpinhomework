import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {HomeComponent} from "./components/common/home/home.component";
import {QuestionBankComponent} from "./components/question-bank/question-bank.component";
import {AdminHomeComponent} from "./components/admin/admin-home/admin-home.component";
import {HomeGuard} from "./guards/home.guard";
import {QbankGuard} from "./guards/qbank.guard";
import {AdminGuard} from "./guards/admin.guard";
import {TutorGuard} from "./guards/tutor.guard";
import {StudentGuard} from "./guards/student.guard";
import {CommonGuard} from "./guards/common.guard";
import {AdminLoginComponent} from "./components/admin-login/admin-login.component";
import {AdminLoginGuard} from "./guards/admin-login.guard";
import {AnswerComponent} from './components/answer/answer.component';
import {ReviewsComponent} from './components/reviews/reviews.component';
import {AboutUsComponent} from "./components/app-content/about-us/about-us.component";
import {HowItWorksComponent} from "./components/app-content/how-it-works/how-it-works.component";
import {PricingComponent} from "./components/app-content/pricing/pricing.component";
import {RefundPolicyComponent} from "./components/app-content/refund-policy/refund-policy.component";
import {FaqComponent} from "./components/app-content/faq/faq.component";
import {ContactUsComponent} from "./components/app-content/contact-us/contact-us.component";
import {PrivacyPolicyComponent} from "./components/app-content/privacy-policy/privacy-policy.component";
import {TermsAndConditionsComponent} from "./components/app-content/terms-and-conditions/terms-and-conditions.component";
import {ConnectWithUsComponent} from "./components/app-content/connect-with-us/connect-with-us.component";
import {BecomeATutorComponent} from "./components/common/become-a-tutor/become-a-tutor.component";
import {AskAQuestionComponent} from "./components/common/ask-a-question/ask-a-question.component";
import {UploadAndEarnComponent} from "./components/common/upload-and-earn/upload-and-earn.component";
import {ProfileSettingsComponent} from "./components/common/profile-settings/profile-settings.component";
import {MyAccountComponent} from "./components/common/my-account/my-account.component";
import {ProfileComponent} from "./components/common/profile/profile.component";

const appRoutes: Routes = [
  {path: 'home', component: HomeComponent, canActivate: [HomeGuard]},
  {path: 'question-bank', component: QuestionBankComponent, canActivate: [QbankGuard]},
  {path: 'answer', component: AnswerComponent},
  {path: 'reviews', component: ReviewsComponent},
  {path: 'admin', component: AdminHomeComponent, canActivate: [AdminGuard]},
  {path: 'tutor', component: HomeComponent, canActivate: [TutorGuard]},
  {path: 'student', component: HomeComponent, canActivate: [StudentGuard]},
  {path: 'become-a-tutor', component: BecomeATutorComponent, canActivate: [HomeGuard]},
  {path: 'admin-login', component: AdminLoginComponent, canActivate: [AdminLoginGuard]},
  {path: 'about-us', component: AboutUsComponent},
  {path: 'how-it-works', component: HowItWorksComponent},
  {path: 'pricing', component: PricingComponent},
  {path: 'refund-policy', component: RefundPolicyComponent},
  {path: 'faq', component: FaqComponent},
  {path: 'contact-us', component: ContactUsComponent},
  {path: 'privacy-policy', component: PrivacyPolicyComponent},
  {path: 'terms-and-conditions', component: TermsAndConditionsComponent},
  {path: 'connect-with-us', component: ConnectWithUsComponent},
  {path: 'ask-a-question', component: AskAQuestionComponent, canActivate: [CommonGuard]},
  {path: 'profile-settings', component: ProfileSettingsComponent, canActivate: [CommonGuard]},
  {path: 'my-account', component: MyAccountComponent, canActivate: [CommonGuard]},
  {path: 'profile/:username', component: ProfileComponent, canActivate: [CommonGuard]},
  {path: 'discuss/:questionId', component: AskAQuestionComponent, canActivate: [CommonGuard]},
  {path: 'upload-and-earn', component: UploadAndEarnComponent, canActivate: [CommonGuard]},
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: '**', redirectTo: '/home'}
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, {useHash: false})
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {

}
