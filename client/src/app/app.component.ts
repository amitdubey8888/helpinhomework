import {Component} from '@angular/core';
import {WindowRefService} from "./providers/window-ref.service";
import {CurrentUserService} from "./providers/current-user.service";
import {AccountService} from "./providers/account.service";
import {CommonService} from "./providers/common.service";
import {SigninService} from "./providers/signin.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  private _window: Window;
  private pages = [
    {
      "name": "Question Bank",
      "route": "/question-bank"
    },
    {
      "name": "Ask a Question",
      "route": "/ask-a-question"
    },
    {
      "name": "Become a Tutor",
      "route": "/become-a-tutor"
    },
    {
      "name": "Inbox",
      "route": "/login"
    },
    {
      "name": "Upload & Earn Money",
      "route": "/login"
    },
    {
      "name": "Search",
      "route": "/login"
    }
  ];
  private companyPages = [
    {
      "name": "About Us",
      "route": "/about-us"
    },
    {
      "name": "How It Works",
      "route": "/how-it-works"
    },
    {
      "name": "Pricing",
      "route": "/pricing"
    },
  ];
  private helpPages = [
    {
      "name": "Refund Policy",
      "route": "/refund-policy"
    },
    {
      "name": "F.A.Q.",
      "route": "/faq"
    },
    {
      "name": "Contact Us",
      "route": "/contact-us"
    },
  ];
  private legalPages = [
    {
      "name": "Privacy Policy",
      "route": "/privacy-policy"
    },
    {
      "name": "Terms and Conditions",
      "route": "terms-and-conditions"
    },
    {
      "name": "Connect With Us",
      "route": "connect-with-us"
    },
  ];

  constructor(windowRef: WindowRefService,
              public signin: SigninService,
              private common: CommonService,
              private account: AccountService,
              private currentUser: CurrentUserService) {
    this._window = windowRef.nativeWindow;
    this.pages = this.common.pages;
  }

  public scrollToTop(): void {
    this._window.scrollTo(0, 0);
  }

  public openDialog() {
    this.signin.showDialog()
  }

  public logout() {
    this.account.logout();
  }
}
