import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'subjectFilter'
})
export class SubjectFilterPipe implements PipeTransform {

  transform(value:any[], id:any):any {
    for (let subject of value) {
      if (subject.id === id) {
        return subject.name;
      }
    }
  }

}
