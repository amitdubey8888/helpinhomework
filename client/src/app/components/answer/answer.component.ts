import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AccountService} from "../../providers/account.service";
import {CommonService} from '../../providers/common.service';
import {ApiService} from '../../providers/api.service';
import {CurrentUserService} from '../../providers/current-user.service';
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-answer',
  templateUrl: './answer.component.html',
  styleUrls: ['./answer.component.css']
})
export class AnswerComponent implements OnInit {

  public questionDetails: any;
  public loading: boolean = false;
  public answerDetails: any;
  public paymentStatus = '';
  private password: string;
  public price = 0;
  public user: any;
  public remainingAmount = 0;
  public payFlag = false;
  private inputType: string = 'password';
  private comments: any;
  private comment = '';
  public reviewDetails = '';
  public paymentDetails = {
    cardNumber: '',
    expireDate: '',
    cvv: ''
  };
  public review = '';

  constructor(private common: CommonService,
              private router: Router,
              private api: ApiService,
              private account: AccountService,
              private currentUser: CurrentUserService) {
  }

  ngOnInit() {
    this.user = this.currentUser.info;
    if (this.common.questionDetails !== null) {
      this.questionDetails = this.common.questionDetails;
      console.log("Qestion Details:", this.questionDetails);
      this.getAnswer(this.questionDetails.id);
      this.getPayments();
      this.getComments();
    } else {
      this.router.navigateByUrl('/question-bank');
    }
  }

  getAnswer(id) {
    this.common.showProgressBar();
    this.api.get(`answers?filter[where][questionId]=${id}`)
      .map(res => res.json())
      .subscribe((response) => {
        this.common.hideProgressBar();
        this.answerDetails = response[0];
      }, error => {
        this.common.hideProgressBar();
        this.common.openSnackBar(`Unable to fetch answer! Please check your internet connection`);
      })
  }

  getPayments() {
    this.common.showProgressBar();
    this.api.get(`payments?filter[where][and][0][questionId]=${this.questionDetails.id}&filter[where][and][1][userId]=${this.currentUser.info.id}`)
      .map(res => res.json())
      .subscribe(response => {
        console.log(response);
        this.common.hideProgressBar();
        for (let pay of response) {
          if (pay.status === 'complete') {
            this.paymentStatus = 'complete';
            break;
          }
        }
        if (this.paymentStatus === '') {
          this.price = this.questionDetails.price * 0.4;
        } else if (this.paymentStatus === 'preview') {
          this.price = this.questionDetails.price * 0.6;
        }
        console.log("Payment Status:", this.paymentStatus);
        console.log("Remaining Price:", this.price);
      }, error => {
        this.common.openSnackBar(`Opps some error occurred!`);
        this.common.hideProgressBar();
      });
  }

  doPayment() {
    console.log(this.paymentDetails);
    this.common.showProgressBar();
    const payment = {
      paid: this.price,
      userId: this.currentUser.info.id,
      questionId: this.questionDetails.id,
      status: 'complete'
    }
    this.api.post(`payments`, payment)
      .map(res => res.json())
      .subscribe(response => {
        this.common.hideProgressBar();
        this.remainingAmount = 0;
        this.paymentStatus = 'complete';
      }, error => {
        this.common.hideProgressBar();
        this.common.openSnackBar(`Oops some error occurred!`);
      });
  }


  open(reviewModal) {
    this.common.showProgressBar();
    this.api.get(`reviews?filter[where][and][0][userId]=${this.answerDetails.userId}&filter[where][and][1][questionId]=${this.questionDetails.id}`)
      .map(res => res.json())
      .subscribe(response => {
        console.log(response);
        this.common.hideProgressBar();
        for (let review of response) {
          if (review.optional.studentId === this.currentUser.info.id) {
            this.reviewDetails = review.comment;
            break;
          }
        }
      })
    // this.openedModal = this.modalService.open(reviewModal);
  }

  reviewTutor() {
    console.log(this.review);
    const rvs = {
      "rating": 0,
      "comment": this.review,
      "optional": {
        "studentId": this.currentUser.info.id,
      },
      "questionId": this.questionDetails.id,
      "extras": [],
      "userId": this.questionDetails.optional.selectedTutor
    };
    this.api.post(`reviews`, rvs)
      .map(res => res.json())
      .subscribe(response => {
        this.common.hideProgressBar();
        // this.openedModal.close();
      }, error => {
        this.common.hideProgressBar();
        this.common.openSnackBar(`Oops some error occurred!`);
      })
  }

  openChangePassword(content) {
    // this.openedModal = this.modalService.open(content);
  }

  closeChangePassword(form: NgForm) {
    form.resetForm();
    // this.openedModal.close();
  }

  changePassword(form: NgForm) {
    this.common.showProgressBar();
    this.user.password = this.password;
    this.account.changeAttributes(this.user)
      .map(res => res.json())
      .subscribe(
        success => {
          this.common.hideProgressBar();
          this.common.openSnackBar(`Password successfully changed`);
          this.closeChangePassword(form);
        },
        error => {
          this.common.hideProgressBar();
          let text = JSON.parse(error._body);
          this.common.openSnackBar(`Unable to change password, ${text.error.message}`);
        });
  }

  logout() {
    this.account.logout();
    this.user = null;
    this.router.navigateByUrl('/home');
  }

  getComments() {
    this.common.showProgressBar();
    this.comments = [];
    this.api.get(`comments?filter[where][questionId]=${this.questionDetails.id}&filter[include]=user`)
      .map(res => res.json())
      .subscribe(response => {
        this.comments = response;
        this.common.hideProgressBar();
      }, error => {
        this.common.openSnackBar(`Opps some error occurred!`);
        this.common.hideProgressBar();
      })
  }

  postComment() {
    this.common.showProgressBar();
    let info = {
      message: this.comment,
      userId: this.currentUser.info.id,
      questionId: this.questionDetails.id
    };


    this.api.post(`comments`, info)
      .map(res => res.json())
      .subscribe(response => {
        this.common.hideProgressBar();
        console.log(response);
        this.comment = '';
        this.getComments();
      }, error => {
        this.common.openSnackBar(`Oops some error occurred!`);
        this.common.hideProgressBar();
      });
  }

}
