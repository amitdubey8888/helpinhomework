import {Component, OnInit} from '@angular/core';
import {CurrentUserService} from "../../../providers/current-user.service";
import {DataService} from "../../../providers/data.service";

@Component({
  selector: 'app-my-account',
  templateUrl: './my-account.component.html',
  styleUrls: ['./my-account.component.css']
})
export class MyAccountComponent implements OnInit {

  private selected: string;
  private questions = [];

  constructor(private currentUser: CurrentUserService,
              private data: DataService) {
  }

  ngOnInit() {
    this.selected = 'askedQuestions';
    this.fetchQuestions()
  }

  fetchQuestions() {
    this.data.fetchQuestionsAskedById(this.currentUser.info.id)
      .subscribe(questions => {
        this.questions = questions;
      })
  }
}
