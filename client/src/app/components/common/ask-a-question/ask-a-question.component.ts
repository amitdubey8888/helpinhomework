import {Component, OnDestroy, OnInit} from '@angular/core';
import {CurrentUserService} from "../../../providers/current-user.service";
import {CommonService} from "../../../providers/common.service";
import {ApiService} from "../../../providers/api.service";
import {ActivatedRoute, Router} from "@angular/router";
import {DataService} from "../../../providers/data.service";
import {MdDialog} from "@angular/material";
import {InviteTutorComponent} from "../../dialogs/invite-tutor/invite-tutor.component";
import * as moment from 'moment';

@Component({
  selector: 'app-ask-a-question',
  templateUrl: './ask-a-question.component.html',
  styleUrls: ['./ask-a-question.component.css'],
})

export class AskAQuestionComponent implements OnInit, OnDestroy {

  private activeTabIndex: number;
  private editorOptions: Object = {
    placeholderText: 'Question Description',
    heightMax: 500,
    heightMin: 400
  };
  private question: any = {
    "title": '',
    "description": '',
    "deadline": new Date(),
    "attachments": [],
    "invitations": [],
    "price": null,
    "subjectId": '',
    "userId": this.currentUser.info.id,
    "status": '',
    "optional": {
      selectedTutor: ''
    }
  };
  private invitedTutors = [];
  private questionSubscription;
  private questionId;
  private subjects = [];
  minDate: Date;
  date: Date;
  time: Date;

  constructor(private currentUser: CurrentUserService,
              private api: ApiService,
              private route: ActivatedRoute,
              private router: Router,
              public dialog: MdDialog,
              private data: DataService,
              private common: CommonService) {
  }

  ngOnInit() {
    this.getSubjects();
    this.getStatusOfQuestion();
    this.minDate = new Date;
    this.questionSubscription = this.route.params.subscribe(params => {
      this.questionId = params['questionId'];
      this.data.fetchQuestionById(this.questionId)
        .subscribe(question => {
          if (question[0]) {
            this.question = question[0];
            this.postProcessTime();
            console.log('question', this.question)
          }
        })
    });
  }

  ngOnDestroy() {
    this.questionSubscription.unsubscribe();
  }

  getSubjects() {
    this.common.showProgressBar();
    this.api.get('subjects?filter[where][archived]=false')
      .map(res => res.json())
      .subscribe(response => {
          this.common.hideProgressBar();
          this.subjects = response;
        },
        error => {
          this.common.hideProgressBar();
          // this.common.openSnackBar(`Unable to fetch subjects, Please check your connection!`);
        })
  }

  attachmentsSelect(event) {
    this.common.showProgressBar();
    this.api.uploadFile(event.srcElement.files)
      .then((response: any) => {
        this.common.hideProgressBar();
        // response.status === 'success' ? this.user.optional[file] = response.files[0].fileURI : null;
        console.info('response', response)
      })
      .catch(error => {
        this.common.hideProgressBar();
        // this.user.optional[file] = null;
        this.common.openSnackBar(`Unable to upload file, Please check your connection! ${error}`);
      })
  }

  addQuestion(form) {
    this.common.showProgressBar();
    this.question.status = 'unassigned';
    this.preProcessTime();
    console.log('deadline', this.question.deadline);
    if (this.question.id) {
      this.api.patch('questions/' + this.question.id, this.question)
        .map(res => res.json())
        .subscribe((response) => {
          this.common.hideProgressBar();
          this.activeTabIndex = 1;
          this.common.openSnackBar(`Your question has been updated successfully.`);
        }, error => {
          this.common.hideProgressBar();
          this.common.openSnackBar(`Please check your connection and try again.`, `Unable to post question!`);
        });
    }
    else {
      this.api.post('questions', this.question)
        .map(res => res.json())
        .subscribe((response) => {
          this.common.hideProgressBar();
          this.common.openSnackBar(`Your question has been posted successfully.`);
          this.router.navigate(['/discuss', response.id]);
        }, error => {
          this.common.hideProgressBar();
          this.common.openSnackBar(`Please check your connection and try again.`, `Unable to post question!`);
        });
    }
  }

  getStatusOfQuestion() {
    // this.activeTabIndex = 1
  }

  openInviteTutorDialog() {
    let inviteTutorSelector = this.dialog.open(InviteTutorComponent, {
      height: '60vh',
      width: '80vw',
      data: this.question
    });
    inviteTutorSelector.afterClosed().subscribe(question => {
      if(question)
        this.question = question
    });
  }

  preProcessTime(){
    this.question.deadline = this.date
  }

  postProcessTime() {
    this.date = new Date(this.question.deadline);
  }
}
