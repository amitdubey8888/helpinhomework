import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadAndEarnComponent } from './upload-and-earn.component';

describe('UploadAndEarnComponent', () => {
  let component: UploadAndEarnComponent;
  let fixture: ComponentFixture<UploadAndEarnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadAndEarnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadAndEarnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
