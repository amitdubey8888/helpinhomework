import {Component, OnInit} from '@angular/core';
import {ApiService} from "../../../providers/api.service";
import {CommonService} from "../../../providers/common.service";
import {CurrentUserService} from "../../../providers/current-user.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  reviews = [
    {
      rating: 5,
      comment: "Thanks to the tutor who provided me effective guidance at every step. He helped me in understanding the concepts that I was finding difficult to complete my assignment.",
      question: "You decide project",
      tutor: "Leadvati"
    },
    {
      rating: 5,
      comment: "Thanks to the tutor who provided me effective guidance at every step. He helped me in understanding the concepts that I was finding difficult to complete my assignment.",
      question: "You decide project",
      tutor: "Leadvati"
    },
    {
      rating: 5,
      comment: "Thanks to the tutor who provided me effective guidance at every step. He helped me in understanding the concepts that I was finding difficult to complete my assignment.",
      question: "You decide project",
      tutor: "Leadvati"
    },
    {
      rating: 5,
      comment: "Thanks to the tutor who provided me effective guidance at every step. He helped me in understanding the concepts that I was finding difficult to complete my assignment.",
      question: "You decide project",
      tutor: "Leadvati"
    }
  ];
  activeQuestions = [
    {
      "title": "title",
      "description": "description",
      "deadline": "2017-07-31T03:42:53.581Z",
      "attachments": [
        "string"
      ],
      "bids": [
        {}
      ],
      "invitations": [
        "string"
      ],
      "price": 1,
      "status": "active",
      "archived": false,
      "optional": {},
      "extras": [
        {}
      ],
      "id": "string",
      "subjectId": "string",
      "user": {profile_picture: 'http://lorempixel.com/100/100'}
    }
  ];
  completedQuestions = [
    {
      "title": "title",
      "description": "description",
      "deadline": "2017-07-31T03:42:53.581Z",
      "attachments": [
        "string"
      ],
      "bids": [
        {}
      ],
      "invitations": [
        "string"
      ],
      "price": 1,
      "status": "active",
      "archived": false,
      "optional": {},
      "extras": [
        {}
      ],
      "id": "string",
      "subjectId": "string",
      "user": {profile_picture: 'http://lorempixel.com/100/100'}
    }
  ];

  constructor(private api: ApiService,
              private router:Router,
              private currentUser:CurrentUserService,
              private common: CommonService) {
  }

  ngOnInit() {
    // this.getReviews()
  }

  goTo(route){
    this.router.navigateByUrl(route);
  }

  uploads(data) {
    this.api.uploadFile(data.srcElement.files[0])
      .then(res => {
        console.log('res', res)
      }, err => {
        console.log('err', err)
      })
  }

  getReviews() {
    this.common.showProgressBar();
    this.api.get(`reviews?filter[order]=timestamp%20DESC&filter[limit]=5&filter[include]=user`)
      .map(res => res.json())
      .subscribe(response => {
        this.common.hideProgressBar();
        console.log(response);
        this.reviews = response;
      }, error => {
        this.common.hideProgressBar();
        // this.common.openSnackBar(`Oops some error occurred!`);
      })
  }

  openReview() {
  }
}
