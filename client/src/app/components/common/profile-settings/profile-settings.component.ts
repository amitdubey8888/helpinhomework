import {Component, OnInit} from '@angular/core';
import {CurrentUserService} from "../../../providers/current-user.service";
import {ApiService} from "../../../providers/api.service";
import {CommonService} from "../../../providers/common.service";
import {DialogService} from "../../../providers/dialog.service";
import {AccountService} from "../../../providers/account.service";
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-profile-settings',
  templateUrl: './profile-settings.component.html',
  styleUrls: ['./profile-settings.component.css']
})
export class ProfileSettingsComponent implements OnInit {

  private currentPassword;
  private newPassword;
  private confirmPassword;
  private date: Date;
  private user: any = {
    "first_name": "",
    "last_name": "",
    "name": "",
    "profile_picture": "",
    "phone": "",
    "dob": null,
    "gender": "",
    "optional": {
      "profileLink1": "",
      "profileLink2": "",
      "profileLink3": "",
      "profile_url": "",
      "file1": null,
      "file2": null,
      "subjectsOfExpertise": [],
      "whyHireMe": "",
      "school": ""
    },
    "extras": [],
    "realm": "",
    "username": "",
    "email": "",
    "id": "",
    "paypal_email": ""
  };

  constructor(private api: ApiService,
              private currentUser: CurrentUserService,
              private confirmDialog: DialogService,
              private account: AccountService,
              private common: CommonService) {
  }

  ngOnInit() {
    this.updateUser()
  }

  updateUser() {
    this.user = Object.assign({}, this.user, this.currentUser.info);
    if (this.user.dob)
      this.date = new Date(this.user.dob);
    this.user.optional.profile_url = `${this.api.baseUrl}profile/${this.user.username}`;
  }

  save(form?: NgForm) {
    this.user.username = this.user.username.toLowerCase();
    this.user.dob = this.date || null;
    console.info('user', this.user);
    this.common.showProgressBar();
    this.api.put("users/updateUser", {details: this.user})
      .map(res => res.json())
      .subscribe(success => {
        this.common.hideProgressBar();
        this.currentUser.info = Object.assign({}, this.user);
        switch (this.user.realm) {
          case 'student':
            localStorage.setItem('studentToken', JSON.stringify(this.user));
            break;
          case 'tutor':
            localStorage.setItem('tutorToken', JSON.stringify(this.user));
            break;
          case 'admin':
            localStorage.setItem('adminToken', JSON.stringify(this.user));
            break;
        }
        this.updateUser();
        this.common.openSnackBar('Account updated successfully');
      }, error => {
        this.common.hideProgressBar();
        let text = JSON.parse(error._body);
        this.common.openSnackBar(`Unable to update, ${text.error.message}`);
      });
  }

  profileImageChange(event) {
    this.common.showProgressBar();
    this.api.uploadFile(event.srcElement.files)
      .then((response: any) => {
        this.common.hideProgressBar();
        response.status === 'success' ? this.user.profile_picture = response.files[0].fileURI : null;
        console.warn('response', response)
      })
      .catch(error => {
        this.common.hideProgressBar();
        this.common.openSnackBar(`Unable to upload file, Please check your connection! ${error}`);
      })
  }

  deactivateAccount() {
    this.confirmDialog.open('Are you sure?', 'All the user data would be deleted and will no longer be available in future if you deactivate your account.')
      .subscribe(sure => {
        if (sure) {
          this.common.showProgressBar();
          this.api.delete('users/' + this.user.id)
            .map(res => res.json())
            .subscribe(() => {
              this.common.hideProgressBar();
              this.api.sendMail(
                `Your account has been permanently erased from our database.`,
                `Account deleted`, this.user.email);
              this.common.openSnackBar(`Account Deactivated`);
              this.account.logout();
            }, error => {
              this.common.hideProgressBar();
              this.common.openSnackBar(`Unable to deactivate`);
            });
        }
      });
  }

  changePassword(form: NgForm) {
    console.warn('curr', this.currentPassword, 'new', this.newPassword, 'confirm', this.confirmPassword);
    this.common.showProgressBar();
    let credentials = {
      username: this.user.username.toLowerCase(),
      password: this.currentPassword,
      realm: this.user.realm.toLowerCase()
    };
    this.api.post('users/login', credentials)
      .map(res => res.json())
      .subscribe(success => {
          this.common.hideProgressBar();
          if (success.id) {
            this.user.password = this.confirmPassword;
            this.save();
            form.resetForm();
          }
          else
            this.common.openSnackBar(`Current password does not match our records`);
        },
        error => {
          this.common.hideProgressBar();
          this.common.openSnackBar(`Current password does not match our records`);
        }
      );
  }
}
