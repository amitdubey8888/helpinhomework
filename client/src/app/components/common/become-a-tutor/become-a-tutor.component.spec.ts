import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BecomeATutorComponent } from './become-a-tutor.component';

describe('BecomeATutorComponent', () => {
  let component: BecomeATutorComponent;
  let fixture: ComponentFixture<BecomeATutorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BecomeATutorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BecomeATutorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
