import {Component, OnInit} from '@angular/core';
import {ApiService} from "../../../providers/api.service";
import {CommonService} from "../../../providers/common.service";
import {CurrentUserService} from "../../../providers/current-user.service";
import {NgForm} from "@angular/forms";
import {Router} from "@angular/router";

@Component({
  selector: 'app-become-a-tutor',
  templateUrl: './become-a-tutor.component.html',
  styleUrls: ['./become-a-tutor.component.css']
})

export class BecomeATutorComponent implements OnInit {

  private loading: boolean = false;
  private subjects = [];
  private user: any = {
    "first_name": "",
    "last_name": "",
    "phone": "",
    "status": "requested",
    "archived": false,
    "realm": "tutor",
    "email": "",
    "password": "",
    "optional": {
      profileLink1: '',
      profileLink2: '',
      profileLink3: '',
      file1: null,
      file2: null,
      subjectsOfExpertise: [],
      whyHireMe: null
    },
    "extras": []
  };

  constructor(public api: ApiService,
              private currentUser: CurrentUserService,
              private router: Router,
              private common: CommonService) {
  }

  ngOnInit() {
    this.fetchAllSubjects();
    this.currentUser.info ? this.user = Object.assign(this.user, this.currentUser.info) : null
  }

  private fetchAllSubjects() {
    this.common.showProgressBar();
    this.api.get(`subjects?filter[where][archived]=false`)
      .map(res => res.json())
      .subscribe(
        (response) => {
          this.common.hideProgressBar();
          this.subjects = response;
        },
        error => {
          this.common.hideProgressBar();
          this.common.openSnackBar(`Unable to fetch subjects, Please check your connection!`);
        })
  }

  private requestTutorPrivileges(form: NgForm) {
    this.common.showProgressBar();
    this.subjects.forEach(subject => {
      subject.checked ? this.user.optional.subjectsOfExpertise.push(subject) : null
    });
    console.info('User', this.user);
    if (this.user.id) {
      delete this.user.password;
      this.user.status = 'requested';
      this.api.put('users/updateUser', {details: this.user})
        .map(res => res.json())
        .subscribe(success => {
          this.common.hideProgressBar();
          this.sendMailToAdmin();
          this.common.openSnackBar(`Your application was successfully sent to admin`);
          this.router.navigate(['/home']);
          form.resetForm();
        }, error => {
          this.common.hideProgressBar();
          let text = JSON.parse(error._body);
          this.common.openSnackBar(`Unable to send your application! An error occurred ${text.error.message}`);
        })
    } else {
      this.api.post('users', this.user)
        .map(res => res.json())
        .subscribe(success => {
          this.common.hideProgressBar();
          this.sendMailToAdmin();
          this.router.navigate(['/home']);
          form.resetForm();
          this.common.openSnackBar(`Your application was successfully sent to admin`);
        }, error => {
          this.common.hideProgressBar();
          let text = JSON.parse(error._body);
          this.common.openSnackBar(`Unable to send your application! An error occurred ${text.error.message}`);
        })
    }
  }

  private attachmentsSelect(event, file) {
    this.common.showProgressBar();
    this.api.uploadFile(event.srcElement.files)
      .then((response: any) => {
        this.common.hideProgressBar();
        response.status === 'success' ? this.user.optional[file] = response.files[0].fileURI : null;
        console.info('response', response)
      })
      .catch(error => {
        this.common.hideProgressBar();
        this.user.optional[file] = null;
        this.common.openSnackBar(`Unable to upload file, Please check your connection! ${error}`);
      })
  }

  private sendMailToAdmin() {
    this.api.sendMail(
      `${this.user.first_name} ${this.user.last_name}  wants to become a tutor, and is awaiting for your response`,
      `You have received a tutor application`, null, `${this.user.email}`)
  }
}
