import { Component, OnInit } from '@angular/core';
import {CurrentUserService} from "../../../providers/current-user.service";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  constructor(private currentUser:CurrentUserService) { }

  ngOnInit() {
  }

}
