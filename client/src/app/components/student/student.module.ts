import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from "@angular/forms";
import {FroalaEditorModule, FroalaViewModule} from "angular-froala-wysiwyg";

import {StudentQuestionsComponent} from './student-questions/student-questions.component';
import {StudentMenuComponent} from "./student-menu/student-menu.component";
import {StudentProfileComponent} from './student-profile/student-profile.component';
import {StudentPaymentsComponent} from './student-payments/student-payments.component';
import {StudentRoutingModule} from "./student-routing.module";
import {AnswerComponent} from './answer/answer.component';
import {StudentQnaComponent} from './student-qna/student-qna.component';
import {MaterialModule} from "../../material/material.module";
import {FlexLayoutModule} from "@angular/flex-layout";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

@NgModule({
  imports: [
    CommonModule,
    StudentRoutingModule,
    FormsModule,
    FroalaEditorModule.forRoot(),
    FroalaViewModule.forRoot(),
    FlexLayoutModule,
    BrowserAnimationsModule,
    MaterialModule
  ],
  declarations: [
    StudentQuestionsComponent,
    StudentMenuComponent,
    StudentProfileComponent,
    StudentPaymentsComponent,
    AnswerComponent,
    StudentQnaComponent,
  ]
})
export class StudentModule {
}
