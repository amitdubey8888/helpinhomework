import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ApiService} from '../../../providers/api.service';
import {CommonService} from '../../../providers/common.service';
import {CurrentUserService} from '../../../providers/current-user.service';

@Component({
  selector: 'app-student-questions',
  templateUrl: './student-questions.component.html',
  styleUrls: ['./student-questions.component.css']
})

export class StudentQuestionsComponent implements OnInit {

  private questionsList = [];
  private ansQuestionsList = [];
  private tutorList = [];
  private openedModal: any;
  private selectedQuestion = {
    id: ''
  };
  private loading: boolean = false;
  private payment = 0;
  private tutor: any;
  private message: string;
  private messages = [];

  constructor(private api: ApiService,
              private router: Router,
              private common: CommonService,
              private currentUser: CurrentUserService) {
  }

  ngOnInit() {
    this.getQuestions();
  }

  doNotShow() {
    this.common.openSnackBar(`You can not see answer. Pay atleast 40% to see preview!`);
  }

  seePreview() {
    this.common.openSnackBar(`You can only see preview answer of this answer!`);
  }

  seeDetails() {
    this.common.openSnackBar(`You can see complete answer of this question!`);
  }

  getQuestions() {
    this.common.showProgressBar();
    this.api.get(`questions?filter[where][and][0][archived]=false&filter[where][and][1][userId]=${this.currentUser.info.id}&filter[include]=subject`)
      .map(res => res.json())
      .subscribe((response) => {
        this.questionsList = response;
        // this.ansQuestionsList = [];
        // for (let qs of response) {
        //   if (qs.status === 'answered' || qs.status === 'completed') {
        //     this.ansQuestionsList.push(qs);
        //     console.log(this.ansQuestionsList);
        //   } else {
        //     this.questionsList.push(qs);
        //     console.log(this.questionsList);
        //   }
        // }
        this.common.hideProgressBar();
      }, error => {
        this.common.hideProgressBar();
        this.common.openSnackBar(`Unable to fetch questions. Please check your connection!`)
      })
  }

  editQuestion(question) {
    this.common.editQuestionID = question.id;
    this.router.navigateByUrl('/student/ask-a-question');
  }

  deleteQuestion(question) {
    // swal({
    //   title: 'Are you sure?',
    //   text: `Are you sure you want to delete this question?`,
    //   type: 'warning',
    //   showCancelButton: true,
    //   confirmButtonText: `Yes, I'm sure!`,
    //   cancelButtonText: 'No, let it be'
    // }).then((success) => {
      this.common.showProgressBar();
      question.archived = true;
      this.api.patch('questions/' + question.id, question)
        .map(res => res.json())
        .subscribe((response) => {
          this.common.hideProgressBar();
          this.getQuestions();
        }, error => {
          this.common.hideProgressBar();
          this.common.openSnackBar(`Unable to delete question. Please check your connection!`)
        });
    // }, error => {
    // });
  }

  getAllTutor(qs, modalName) {
    this.selectedQuestion = qs;
    this.common.showProgressBar();
    this.api.get('users?filter[where][realm]=tutor')
      .map(res => res.json())
      .subscribe((response) => {
        this.tutorList = [];
        for (let tutor of response) {
          for (let bid of qs.bids) {
            if (bid.userId === tutor.id) {
              tutor.price = bid.price;
              this.tutorList.push(tutor);
            }
          }
        }
        this.common.hideProgressBar();
        this.open(modalName);
      }, error => {
        this.common.openSnackBar(`Oops! An error occurred`);
        this.common.hideProgressBar();
      })
  }

  open(modalName) {
    // this.openedModal = this.modalService.open(modalName);
  }

  selectTutor(tutor, qs) {
    // swal({
    //   title: 'Are you sure?',
    //   text: `Do you really want to select this tutor?`,
    //   type: 'warning',
    //   showCancelButton: true,
    //   confirmButtonText: `Yes, I'm sure!`,
    //   cancelButtonText: 'No, let it be'
    // }).then((success) => {
      qs.optional = {
        selectedTutor: tutor.id,
        bidPrice: tutor.price
      };
      // this.openedModal.close();
      this.common.showProgressBar();
      this.api.patch('questions/' + qs.id, qs)
        .map(res => res.json())
        .subscribe((response) => {
          this.common.hideProgressBar();
          this.getQuestions();
        }, error => {
          this.common.hideProgressBar();
          this.common.openSnackBar(`Oops! An error occurred!`)
        });
    // }, error => {
    // });
  }

  seeAnswer(qs) {
    this.common.questionDetails = qs;
    this.router.navigateByUrl('/student/answer');
  }

  processPayment(val: number) {
    console.log("payment of ", val);
  }

  talk(qs, talkModal) {
    this.common.showProgressBar();
    this.selectedQuestion = qs;
    this.api.get(`users/${qs.optional.selectedTutor}`)
      .map(res => res.json())
      .subscribe(response => {
        this.tutor = response;
        this.common.hideProgressBar();
      }, error => {
        this.common.openSnackBar(`Oops some error occurred!`);
        this.common.hideProgressBar();
      });
    this.api.get(`comments?filter[where][questionId]=${qs.id}`)
      .map(res => res.json())
      .subscribe(response => {
        this.messages = response;
        this.common.hideProgressBar();
      }, error => {
        this.common.openSnackBar(`Opps some error occurred!`);
        this.common.hideProgressBar();
      })
    // this.openedModal = this.modalService.open(talkModal);
  }

  send() {
    this.common.showProgressBar();
    let comment = {
      message: this.message,
      userId: this.currentUser.info.id,
      questionId: this.selectedQuestion.id
    };

    this.api.post(`comments`, comment)
      .map(res => res.json())
      .subscribe(response => {
        this.common.hideProgressBar();
        this.messages.push(comment);
        this.message = null;
      }, error => {
        this.common.openSnackBar(`Oops some error occurred!`);
        this.common.hideProgressBar();
      });
  }
}
