import {NgModule} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";

import {StudentMenuComponent} from "./student-menu/student-menu.component";
import {HomeComponent} from "../common/home/home.component";
import {StudentProfileComponent} from "./student-profile/student-profile.component";
import {StudentPaymentsComponent} from "./student-payments/student-payments.component";
import {StudentQnaComponent} from "./student-qna/student-qna.component";
import {AnswerComponent} from "./answer/answer.component";
import {StudentGuard} from "../../guards/student.guard";
import {ProfileSettingsComponent} from "../common/profile-settings/profile-settings.component";
import {CommonGuard} from "../../guards/common.guard";

const studentRoutes: Routes = [{
  path: 'student', component: StudentMenuComponent,
  children: [
    // {path: 'profile', component: StudentProfileComponent, canActivate: [StudentGuard]},
    // {path: 'payments', component: StudentPaymentsComponent, canActivate: [StudentGuard]},
    {path: 'home', component: HomeComponent, canActivate: [StudentGuard]},
    // {path: 'answer', component: AnswerComponent, canActivate: [StudentGuard]},
    // {path: 'qna', component: StudentQnaComponent, canActivate: [StudentGuard]},
    // {path: 'profile-settings', component: ProfileSettingsComponent, canActivate: [CommonGuard]},
    {path: '', redirectTo: '/student/home', pathMatch: 'full'},
    {path: '**', redirectTo: '/student/home'}
  ]
}];

@NgModule({
  imports: [
    RouterModule.forChild(studentRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class StudentRoutingModule {
}
