import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from '../../../providers/common.service';
import { ApiService } from '../../../providers/api.service';
import { CurrentUserService } from '../../../providers/current-user.service';

@Component({
  selector: 'app-answer',
  templateUrl: './answer.component.html',
  styleUrls: ['./answer.component.css']
})
export class AnswerComponent implements OnInit {

  public questionDetails: any;
  public loading: boolean = false;
  public answerDetails: any;
  public paymentStatus = '';
  public price = 0;
  public payedAmount = 0;
  public openedModal: any;
  public reviewDetails = {
    comment: '',
    rating: 1
  };
  public tutorsList = [];
  public payFlag = false;
  public comments: any;
  public comment = '';
  public user: any;
  public paymentDetails = {
    cardNumber: '',
    expireDate: '',
    cvv: ''
  };
  public review = {
    text: '',
    rating: 1
  };

  constructor(private common: CommonService,
    private router: Router,
    private api: ApiService,
    private currentUser: CurrentUserService) {
  }

  ngOnInit() {
    this.user = this.currentUser.info;
    if (this.common.questionDetails !== null) {
      this.questionDetails = this.common.questionDetails;
      if (!this.questionDetails.optional.selectedTutor) {
        this.getAllTutor();
      } else {
        this.getAnswer(this.questionDetails.id);
        this.getPayments();
        this.getComments();
      }

    } else {
      this.router.navigateByUrl('/student/my-questions');
    }
  }

  getAnswer(id) {
    this.common.showProgressBar();
    this.api.get(`answers?filter[where][questionId]=${id}`)
      .map(res => res.json())
      .subscribe((response) => {
        this.common.hideProgressBar();
        this.answerDetails = response[0];
        console.log("Answer", this.answerDetails);
      }, error => {
        this.common.hideProgressBar();
        this.common.openSnackBar(`Unable to fetch answer! Please check your internet connection`);
      })
  }

  getPayments() {
    this.common.showProgressBar();
    this.api.get(`payments?filter[where][and][0][questionId]=${this.questionDetails.id}&filter[where][and][1][userId]=${this.currentUser.info.id}`)
      .map(res => res.json())
      .subscribe(response => {
        console.log(response);
        this.common.hideProgressBar();
        for (let pay of response) {
          if (pay.status === 'complete') {
            this.paymentStatus = 'complete';
            this.payedAmount = 60;
            break;
          } else if (pay.status === 'preview') {
            this.payedAmount = 40;
            this.paymentStatus = 'preview';
          }
        }
        if (this.paymentStatus === '') {
          this.price = parseFloat((this.questionDetails.optional.bidPrice * 0.4).toFixed(2));
        } else if (this.paymentStatus === 'preview') {
          this.price = parseFloat((this.questionDetails.optional.bidPrice * 0.6).toFixed(2));
        }
        console.log("Payment Status:", this.paymentStatus);
        console.log("Remaining Price:", this.price);
      }, error => {
        this.common.openSnackBar(`Opps some error occurred!`);
        this.common.hideProgressBar();
      });
  }

  doPayment() {
    console.log(this.paymentDetails);
    this.common.showProgressBar();
    const payment = {
      paid: this.price,
      userId: this.currentUser.info.id,
      questionId: this.questionDetails.id,
      receiver: this.questionDetails.optional.selectedTutor,
      status: this.paymentStatus ? 'complete' : 'preview'
    };
    this.api.post(`payments`, payment)
      .map(res => res.json())
      .subscribe(response => {
        this.common.hideProgressBar();
        this.payedAmount = 0;
        this.payFlag = false;
        this.paymentStatus = this.paymentStatus ? 'complete' : 'preview';
        if (this.paymentStatus === 'preview') {
          this.payedAmount = 40;

          this.price = this.questionDetails.optional.bidPrice * 0.6;
          this.updateAnswer('previewed');
        } else if (this.paymentStatus === 'complete') {
          this.payedAmount = 60;

          this.updateAnswer('completed');
          this.updateQuestion();
        }
      }, error => {
        this.common.hideProgressBar();
        this.common.openSnackBar(`Oops some error occurred!`);
      });
  }

  updateQuestion() {
    this.common.showProgressBar();
    this.questionDetails.status = "completed";
    this.api.patch('questions/' + this.questionDetails.id, this.questionDetails)
      .map(res => res.json())
      .subscribe((response) => {
        this.common.hideProgressBar();
      }, error => {
        this.common.hideProgressBar();
        this.common.openSnackBar(`Some error occurred. Unable to update`);
      });
  }

  updateAnswer(status) {
    this.common.showProgressBar();
    this.answerDetails.status = status;
    this.api.patch('answers/' + this.answerDetails.id, this.answerDetails)
      .map(res => res.json())
      .subscribe((response) => {
        this.common.hideProgressBar();
      }, error => {
        this.common.hideProgressBar();
        this.common.openSnackBar(`Some error occurred. Unable to update`);
      });
  }

  open(reviewModal) {
    this.common.showProgressBar();
    this.api.get(`reviews?filter[where][and][0][userId]=${this.answerDetails.userId}&filter[where][and][1][questionId]=${this.questionDetails.id}`)
      .map(res => res.json())
      .subscribe(response => {
        console.log(response);
        this.common.hideProgressBar();
        for (let review of response) {
          if (review.optional.studentId === this.currentUser.info.id) {
            this.reviewDetails = {
              comment: review.comment,
              rating: review.rating
            };
            break;
          }
        }
      })
    // this.openedModal = this.modalService.open(reviewModal);
  }

  reviewTutor() {
    this.common.showProgressBar();
    const rvs = {
      "rating": this.review.rating,
      "comment": this.review.text,
      "optional": {
        "studentId": this.currentUser.info.id,
      },
      "questionId": this.questionDetails.id,
      "extras": [],
      "userId": this.questionDetails.optional.selectedTutor
    };
    this.api.post(`reviews`, rvs)
      .map(res => res.json())
      .subscribe(response => {
        this.common.hideProgressBar();
        this.openedModal.close();
      }, error => {
        this.common.hideProgressBar();
        this.common.openSnackBar(`Oops some error occurred!`);
      });
  }

  getAllTutor() {
    this.common.showProgressBar();
    this.api.get('users?filter[where][realm]=tutor')
      .map(res => res.json())
      .subscribe((response) => {
        this.tutorsList = [];
        for (let tutor of response) {
          for (let bid of this.questionDetails.bids) {
            if (bid.userId === tutor.id) {
              tutor.price = bid.price;
              this.tutorsList.push(tutor);
            }
          }
        }
        this.common.hideProgressBar();
      }, error => {
        this.common.openSnackBar(`Oops! An error occurred`);
        this.common.hideProgressBar();
      })
  }

  selectTutor(tutor) {
    // swal({
    //   title: 'Are you sure?',
    //   text: `Do you really want to select this tutor?`,
    //   type: 'warning',
    //   showCancelButton: true,
    //   confirmButtonText: `Yes, I'm sure!`,
    //   cancelButtonText: 'No, let it be'
    // }).then((success) => {
      this.questionDetails.optional = {
        selectedTutor: tutor.id,
        bidPrice: tutor.price
      };
      this.common.showProgressBar();
      this.api.patch('questions/' + this.questionDetails.id, this.questionDetails)
        .map(res => res.json())
        .subscribe((response) => {
          this.common.hideProgressBar();
        }, error => {
          this.common.hideProgressBar();
          this.common.openSnackBar(`Oops! An error occurred!`)
        });
    // }, error => {
    // });
  }


  getComments() {
    this.common.showProgressBar();
    this.comments = [];
    this.api.get(`comments?filter[where][questionId]=${this.questionDetails.id}&filter[include]=user`)
      .map(res => res.json())
      .subscribe(response => {
        this.comments = response;
        this.common.hideProgressBar();
      }, error => {
        this.common.openSnackBar(`Opps some error occurred!`);
        this.common.hideProgressBar();
      })
  }

  postComment() {
    this.common.showProgressBar();
    let info = {
      message: this.comment,
      userId: this.currentUser.info.id,
      questionId: this.questionDetails.id
    };


    this.api.post(`comments`, info)
      .map(res => res.json())
      .subscribe(response => {
        this.common.hideProgressBar();
        console.log(response);
        this.comment = '';
        this.getComments();
      }, error => {
        this.common.openSnackBar(`Oops some error occurred!`);
        this.common.hideProgressBar();
      });
  }


}
