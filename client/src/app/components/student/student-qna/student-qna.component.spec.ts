import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentQnaComponent } from './student-qna.component';

describe('StudentQnaComponent', () => {
  let component: StudentQnaComponent;
  let fixture: ComponentFixture<StudentQnaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentQnaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentQnaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
