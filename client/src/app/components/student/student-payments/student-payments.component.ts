import {Component, OnInit} from '@angular/core';
import {ApiService} from "../../../providers/api.service";
import {CurrentUserService} from "../../../providers/current-user.service";
import {CommonService} from "../../../providers/common.service";

@Component({
  selector: 'app-student-payments',
  templateUrl: './student-payments.component.html',
  styleUrls: ['./student-payments.component.css']
})
export class StudentPaymentsComponent implements OnInit {

  private transactions = [];

  constructor(private api:ApiService,
              private common:CommonService,
              private currentUser:CurrentUserService) {
  }

  ngOnInit() {
    this.getPayments();
  }

  getPayments() {
    this.common.showProgressBar();
    this.api.get(`payments?filter[order]=timestamp%20DESC&filter[include]=question&filter[where][userId]=5937c46b6610937495de17bf`)
    //[where][userId]=${this.currentUser.info.id}&filter
      .map(res => res.json())
      .subscribe(
        (response) => {
          this.common.hideProgressBar();
          this.transactions = response;
          console.log(`transacs`, response, this.currentUser.info.id)
        },
        error => {
          this.common.openSnackBar(`Unable to fetch transactions. Please check your connection!`);
        })
  }

  raiseDispute(transaction) {
    // swal({
    //   title: 'Are you sure?',
    //   text: `A dispute will be raised and admin will be contacted for moderation`,
    //   type: 'warning',
    //   showCancelButton: true,
    //   confirmButtonText: `Yes, I'm sure!`,
    //   cancelButtonText: 'No, let it be'
    // }).then(success => {
      this.common.openSnackBar(`A dispute has been raised successfully`);
    // }, dismiss => {
    // });
  }
}
