// import {Component, OnInit} from '@angular/core';
// import {Router} from '@angular/router';
// import {ApiService} from '../../../providers/api.service';
// import {CommonService} from '../../../providers/common.service';
// import {CurrentUserService} from '../../../providers/current-user.service';
//
// @Component({
//   selector: 'app-ask-a-question',
//   templateUrl: './ask-a-question.component.html',
//   styleUrls: ['./ask-a-question.component.css']
// })
// export class AskAQuestionComponent implements OnInit {
//
//   private editQuestionId = '';
//   private subjects = [];
//   private tutors = '';
//   private attachedFiles = [];
//   private bidFlag = false;
//   private question = {
//     "title": "",
//     "description": "",
//     "deadline": new Date(),
//     "attachments": [],
//     "invitations": [],
//     "price": null,
//     "subjectId": "",
//     "userId": this.currentUser.info.id,
//     "bids": [],
//     "status": "",
//     "optional": {
//       selectedTutor: ''
//     }
//   };
//
//   constructor(private api:ApiService,
//               private router:Router,
//               private common:CommonService,
//               private currentUser:CurrentUserService) {
//   }
//
//   ngOnInit() {
//     this.getSubjects();
//     if (this.common.editQuestionID !== '') {
//       this.editQuestionId = this.common.editQuestionID;
//       this.editQuestion();
//     }
//   }
//
//   getSubjects() {
//     this.common.showProgressBar();
//     this.api.get('subjects?filter[where][archived]=false')
//       .map(res => res.json())
//       .subscribe(response => {
//           this.common.hideProgressBar();
//           this.subjects = response;
//         },
//         error => {
//           this.common.hideProgressBar();
//           this.common.openSnackBar(`Unable to fetch subjects, Please check your connection!`);
//         })
//   }
//
//   addQuestion() {
//     if (this.tutors.length != 0) {
//       this.question.invitations = this.tutors.replace(/\s/g, "").split(",");
//     } else {
//       this.question.invitations = [];
//     }
//     this.common.showProgressBar();
//     this.api.post('questions', this.question)
//       .map(res => res.json())
//       .subscribe((response) => {
//         this.common.hideProgressBar();
//         this.common.openSnackBar(`Your question has been posted successfully.`);
//         this.router.navigateByUrl('/student/my-questions');
//       }, error => {
//         this.common.hideProgressBar();
//         this.common.openSnackBar(`Please check your connection and try again.`, `Unable to post question!`)
//       });
//   }
//
//   editQuestion() {
//     this.common.showProgressBar();
//     this.api.get(`questions/${this.common.editQuestionID}`)
//       .map(res => res.json())
//       .subscribe((response) => {
//         this.common.hideProgressBar();
//         this.question = response;
//         this.tutors = response.invitations.join(",");
//       }, error => {
//         this.common.hideProgressBar();
//         this.common.openSnackBar(`Oops! Some error occurred`);
//       })
//   }
//
//   private attachmentsSelect(event, flag) {
//     this.attachedFiles = event.srcElement.files;
//     this.api.uploadFile(this.attachedFiles[0])
//       .then(res => {
//         console.info('res', res)
//       })
//       .catch(err => {
//         console.error('err', err)
//       })
//   }
//
//   parseDate(dateString:string):Date {
//     if (dateString) {
//       return new Date(dateString);
//     } else {
//       return null;
//     }
//   }
//
//   private updateQuestion() {
//     if (this.tutors.length != 0) {
//       this.question.invitations = this.tutors.replace(/\s/g, "").split(",");
//     } else {
//       this.question.invitations = [];
//     }
//     this.common.showProgressBar();
//     this.api.patch('questions/' + this.common.editQuestionID, this.question)
//       .map(res => res.json())
//       .subscribe(
//         (response) => {
//           this.common.hideProgressBar();
//           this.common.editQuestionID = '';
//           this.router.navigateByUrl('/student/my-questions');
//           this.common.openSnackBar(`Question successfully updated.`);
//         },
//         error => {
//           this.common.hideProgressBar();
//           this.common.openSnackBar(`Please check your connection and try again.`, `Unable to post question!`);
//         });
//   }
// }
