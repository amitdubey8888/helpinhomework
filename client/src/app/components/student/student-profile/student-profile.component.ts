import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ApiService} from '../../../providers/api.service';
import {CommonService} from '../../../providers/common.service';
import {CurrentUserService} from "../../../providers/current-user.service";


@Component({
  selector: 'app-student-profile',
  templateUrl: './student-profile.component.html',
  styleUrls: ['./student-profile.component.css']
})
export class StudentProfileComponent implements OnInit {

  private editFlag:boolean = false;
  private newProfileImage:any;
  private user = {
    "first_name": "",
    "last_name": "",
    "profile_picture": "",
    "gender": "",
    "dob": "",
    "phone": "",
    "alternate_email": "",
    "username": "",
    "email": ""
  };
  private today:string = new Date().toString();
  private minDate:string = new Date("January 1, 1990 12:00:00").toString();

  constructor(private api:ApiService,
              private currentUser:CurrentUserService,
              private common:CommonService) {
  }

  ngOnInit() {
    this.user = this.currentUser.info;
  }

  save(form) {
    this.user.username = this.user.username.toLowerCase();
    this.common.showProgressBar();
    this.api.put("users/updateUser", {details: this.user})
      .map(res => res.json())
      .subscribe(
        success => {
          this.common.hideProgressBar();
          this.currentUser.info = this.user;
          this.editFlag = false;
          this.common.openSnackBar('Account updated successfully');
        },
        error => {
          this.common.hideProgressBar();
          let text = JSON.parse(error._body);
          this.common.openSnackBar(`Unable to update, ${text.error.message}`);
        });
  }

  parseDate(dateString:string):Date {
    if (dateString) {
      return new Date(dateString);
    } else {
      return null;
    }
  }

  private attachmentsSelect(event) {
    this.newProfileImage = event.srcElement.files[0];
  }
}
