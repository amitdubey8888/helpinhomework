import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {ApiService} from '../../../providers/api.service';
import {CommonService} from '../../../providers/common.service';
import {MdDialog, MdPaginator, MdSort} from "@angular/material";
import {TutorsDatabase} from "./tutors-database";
import {TutorDataSource} from "./tutors-datasource";
import {Observable} from "rxjs/Observable";
import {ViewUserComponent} from "../../dialogs/view-user/view-user.component";

@Component({
  selector: 'app-admin-tutor',
  templateUrl: './admin-tutors.component.html',
  styleUrls: ['./admin-tutors.component.css']
})
export class AdminTutorComponent implements OnInit {

  private activeTabIndex: number;
  displayedColumns = ['userName', 'email', 'joined', 'action'];
  private tutorTabs = [
    {
      label: "REQUESTS",
      status: 'requested'
    },
    {
      label: "PRIVILEGED",
      status: 'privileged'
    },
    {
      label: "UNPRIVILEGED",
      status: 'unprivileged'
    },
    {
      label: "DISABLED",
      status: 'disabled'
    }
  ];
  tutorsDatabase = new TutorsDatabase();
  dataSource: TutorDataSource | null;

  @ViewChild(MdSort) sort: MdSort;
  @ViewChild(MdPaginator) paginator: MdPaginator;
  @ViewChild('filter') filter: ElementRef;

  constructor(public api: ApiService,
              public router: Router,
              public dialog: MdDialog,
              public common: CommonService) {
  }

  ngOnInit() {
    this.activeTabIndex = 0;
    this.fetchTutors();
    this.dataSource = new TutorDataSource(this.tutorsDatabase, this.paginator, this.sort);
    Observable.fromEvent(this.filter.nativeElement, 'keyup')
      .debounceTime(150)
      .distinctUntilChanged()
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      });
  }

  fetchTutors() {
    this.common.showProgressBar();
    this.api.get(`users?filter[where][and][0][realm]=tutor&filter[where][and][1][status]=${this.tutorTabs[this.activeTabIndex].status}`)
      .map(res => res.json())
      .subscribe(tutors => {
          this.common.hideProgressBar();
          this.tutorsDatabase.deleteAllUsers();
          tutors.forEach(tutor => this.tutorsDatabase.addUser(tutor))
        },
        error => {
          this.common.hideProgressBar();
          this.common.openSnackBar('Error while fetching pending tutors')
        })
  }

  acceptTutorApplication(tutor) {
    tutor.status = 'privileged';
    this.common.showProgressBar();
    this.api.put("users/updateUser", {details: tutor})
      .map(res => res.json())
      .subscribe(success => {
          this.common.hideProgressBar();
          this.api.sendMail(
            `Your application to become a tutor has been approved. You may now login to your account and access the privileges.`,
            `Tutor Application Approved`, tutor.email);
          this.tutorsDatabase.deleteUser(tutor);
          this.common.openSnackBar(`Application approved for ${tutor.name}`)
        },
        error => {
          this.common.hideProgressBar();
          let text = JSON.parse(error._body);
          this.common.openSnackBar(`Unable to approve, ${text.error.message}`);
        });
  }

  rejectTutorApplication(tutor, message) {
    tutor.status = 'unprivileged';
    this.common.showProgressBar();
    this.api.put("users/updateUser", {details: tutor})
      .map(res => res.json())
      .subscribe(success => {
          this.common.hideProgressBar();
          this.api.sendMail(
            `Your application to become a tutor has been rejected.`,
            `Tutor Application Rejected`, tutor.email);
          this.tutorsDatabase.deleteUser(tutor);
          this.common.openSnackBar(`Application approved for ${tutor.name}`);
          this.tutorsDatabase.deleteUser(tutor);
          this.common.openSnackBar(`${message} for ${tutor.name}`);
        },
        error => {
          this.common.hideProgressBar();
          let text = JSON.parse(error._body);
          this.common.openSnackBar(`Unable to approve, ${text.error.message}`);
        });
  }

  deleteTutor(tutor, list) {
    this.common.showProgressBar();
    this.api.delete('users/' + tutor.id)
      .map(res => res.json())
      .subscribe(
        (response) => {
          this.common.hideProgressBar();
          this.api.sendMail(
            `Your account has been permanently erased from our database.`,
            `Account deleted`, tutor.email);
          this.tutorsDatabase.deleteUser(tutor);
          this.common.openSnackBar(`${tutor.name} Deleted`);
        }, error => {
          this.common.hideProgressBar();
          this.common.openSnackBar(`Unable to delete ${tutor.name}`);
          console.log('Unable to delete');
        });
  }

  disableTutor(tutor, days) {
    this.common.showProgressBar();
    tutor.status = 'disabled';
    tutor.optional = {
      disabledDays: days
    };
    this.api.put("users/updateUser", {details: tutor})
      .map(res => res.json())
      .subscribe(
        success => {
          this.common.hideProgressBar();
          this.tutorsDatabase.deleteUser(tutor);
          this.common.openSnackBar(`Account disabled for ${tutor.name}!`);
        },
        error => {
          this.common.hideProgressBar();
          let text = JSON.parse(error._body);
          this.common.openSnackBar(`An error occurred, ${text.error.message}`);
        });
  }

  viewTutor(tutor) {
    console.log(tutor);
    let viewTutorSelector = this.dialog.open(ViewUserComponent, {
      height: '60vh',
      width: '80vw',
      data: tutor
    });
    viewTutorSelector.afterClosed().subscribe(selected => {
      // selected === 'tutor' ? this.router.navigateByUrl('/tutor-login') : this.router.navigateByUrl('/login')
    });
  }

  enableTutor(tutor) {
    this.common.showProgressBar();
    tutor.status = 'unprivileged';
    this.api.put("users/updateUser", {details: tutor})
      .map(res => res.json())
      .subscribe(
        success => {
          this.common.hideProgressBar();
          this.tutorsDatabase.deleteUser(tutor);
          this.common.openSnackBar(`Account enabled for ${tutor.name}!`);
        },
        error => {
          this.common.hideProgressBar();
          let text = JSON.parse(error._body);
          this.common.openSnackBar(`An error occurred, ${text.error.message}`);
        });
  }
}
