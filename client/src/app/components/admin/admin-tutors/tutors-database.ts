/**
 * Created by shubhamjaiswal on 26/07/17.
 */
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {TutorModel} from "./tutor-model";

/** An example database that the data source uses to retrieve data for the table. */
export class TutorsDatabase {
  /** Stream that emits whenever the data has been modified. */
  dataChange: BehaviorSubject<TutorModel[]> = new BehaviorSubject<TutorModel[]>([]);

  get data(): TutorModel[] {
    return this.dataChange.value;
  }

  constructor() {
  }

  /** Adds a new user to the database. */
  addUser(tutor) {
    const copiedData = this.data.slice();
    copiedData.push(TutorsDatabase.createNewUser(tutor));
    this.dataChange.next(copiedData);
  }

  deleteUser(tutor) {
    let copiedData = this.data.slice();
    // copiedData.push(TutorsDatabase.createNewUser(tutor));
    copiedData = copiedData.filter(ptutor => {
      return ptutor.id != tutor.id
    });
    this.dataChange.next(copiedData);
  }

  deleteAllUsers() {
    this.dataChange.next([]);
  }

  /** Builds and returns a new User. */
  private static createNewUser(tutor) {
    return {
      id: tutor.id,
      name: `${tutor.first_name} ${tutor.last_name}`,
      email: tutor.email,
      profile_picture: tutor.profile_picture,
      username: tutor.username,
      createdAt: tutor.createdAt,
      status: tutor.status
    };
  }
}
