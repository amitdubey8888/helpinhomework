/**
 * Created by shubhamjaiswal on 26/07/17.
 */
export interface TutorModel {
  id: string;
  name: string;
  email: string;
  profile_picture: string;
  username: string;
  createdAt: string;
  status: string;
}
