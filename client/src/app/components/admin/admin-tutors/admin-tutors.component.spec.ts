import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminTutorComponent } from './admin-tutors.component';

describe('AdminTutorComponent', () => {
  let component: AdminTutorComponent;
  let fixture: ComponentFixture<AdminTutorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminTutorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminTutorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
