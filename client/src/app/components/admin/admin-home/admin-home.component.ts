import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ApiService} from '../../../providers/api.service';
import {CommonService} from '../../../providers/common.service';
import {CurrentUserService} from "../../../providers/current-user.service";

@Component({
  selector: 'app-admin-home',
  templateUrl: './admin-home.component.html',
  styleUrls: ['./admin-home.component.css']
})
export class AdminHomeComponent implements OnInit {

  private date = new Date();
  private tutors = [];
  private activeTutorsCount = null;
  private students = [];
  private activeStudentsCount = null;
  private subjects = [];
  private activeSubjectsCount = null;
  private totalPayments = null;
  private transactions = [];
  private admin = {
    "first_name": "",
    "last_name": "",
    "profile_picture": "",
    "gender": "",
    "phone": "",
    "alternate_email": "",
    "username": "",
    "email": "",
    "password": ""
  };

  constructor(public api:ApiService,
              public router:Router,
              public currentUser:CurrentUserService,
              public common:CommonService) {
  }

  ngOnInit() {
    this.getTutors();
    this.getStudents();
    // this.getSubjects();
    this.getTransactions();
    this.admin = this.currentUser.info;
  }

  getTutors() {
    this.common.showProgressBar();
    this.api.get('users?filter[where][realm]=tutor')
      .map(res => res.json())
      .subscribe((response) => {
        this.tutors = response;
        this.activeTutorsCount = this.tutors.map(tutor => {
          return tutor.status == "active" ? 1 : 0
        }).reduce((sum, n) => sum + n, 0);
        this.common.hideProgressBar();
      }, error => {
        this.common.openSnackBar(`Unable to fetch tutors info. Please check your connection!`);
        this.common.hideProgressBar();
      })
  }

  getStudents() {
    this.common.showProgressBar();
    this.api.get('users?filter[where][realm]=student')
      .map(res => res.json())
      .subscribe((response) => {
        this.students = response;
        this.activeStudentsCount = this.students.map(student => {
          return student.archived == false ? 1 : 0
        }).reduce((sum, n) => sum + n, 0);
        this.common.hideProgressBar();
      }, error => {
        this.common.openSnackBar(`Unable to fetch students info. Please check your connection!`);
        this.common.hideProgressBar();
      })
  }

  getSubjects() {
    this.common.showProgressBar();
    this.api.get('subjects')
      .map(res => res.json())
      .subscribe((response) => {
        this.subjects = response;
        this.activeSubjectsCount = this.subjects.map(subject => {
          return subject.archived == false ? 1 : 0
        }).reduce((sum, n) => sum + n, 0);
        this.common.hideProgressBar();
      }, error => {
        this.common.openSnackBar(`Unable to fetch subjects info. Please check your connection!`);
        this.common.hideProgressBar();
      })
  }

  getTransactions() {
    this.common.showProgressBar();
    this.api.get(`payments`)
      .map(res => res.json())
      .subscribe(
        (response) => {
          this.common.hideProgressBar();
          this.transactions = response;
          this.totalPayments = response
            .map(transaction => {
              return transaction.paid
            })
            .reduce((sum, n) => sum + n, 0);
        },
        error => {
          this.common.openSnackBar(`Unable to fetch transactions. Please check your connection!`);
        })
  }
}
