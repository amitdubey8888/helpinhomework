import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {ApiService} from '../../../providers/api.service';
import {CommonService} from '../../../providers/common.service';
import {MdPaginator, MdSort} from "@angular/material";
import {SubjectsDatabase} from "./subjects-database";
import {SubjectDataSource} from "./subjects-datasource";
import {Observable} from "rxjs/Observable";

@Component({
  selector: 'app-admin-subjects',
  templateUrl: './admin-subjects.component.html',
  styleUrls: ['./admin-subjects.component.css']
})

export class AdminSubjectsComponent implements OnInit {

  private loading: boolean = false;
  displayedColumns = ['name', 'action'];
  subjectsDatabase = new SubjectsDatabase();
  dataSource: SubjectDataSource | null;
  private subject: any = {
    name: ''
  };

  @ViewChild(MdSort) sort: MdSort;
  @ViewChild(MdPaginator) paginator: MdPaginator;
  @ViewChild('filter') filter: ElementRef;

  constructor(public api: ApiService,
              public router: Router,
              public common: CommonService) {
  }

  ngOnInit() {
    // this.activeTabIndex = 0;
    this.fetchSubjects();
    this.dataSource = new SubjectDataSource(this.subjectsDatabase, this.paginator, this.sort);
    Observable.fromEvent(this.filter.nativeElement, 'keyup')
      .debounceTime(150)
      .distinctUntilChanged()
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      });
  }

  fetchSubjects() {
    this.common.showProgressBar();
    this.api.get(`subjects?filter[where][archived]=false`)
      .map(res => res.json())
      .subscribe(subjects => {
          this.common.hideProgressBar();
          this.subjectsDatabase.deleteAllSubjects();
          subjects.forEach(subject => {
            if(subject.name)
            this.subjectsDatabase.addSubject(subject)
          })
        },
        error => {
          this.common.hideProgressBar();
          this.common.openSnackBar('Error while fetching subjects')
        })
  }

  deleteSubject(subject) {
    this.common.showProgressBar();
    this.api.delete('subjects/' + subject.id)
      .map(res => res.json())
      .subscribe(response => {
        this.common.hideProgressBar();
        this.subjectsDatabase.deleteSubject(subject);
        this.common.openSnackBar(`${subject.name} deleted`);
      }, error => {
        this.common.hideProgressBar();
        this.common.openSnackBar(`Unable to delete ${subject.name}`);
      });
  }

  addSubject() {
    this.common.showProgressBar();
    this.api.post('subjects', this.subject)
      .map(res => res.json())
      .subscribe(response => {
        this.common.hideProgressBar();
        this.subjectsDatabase.addSubject(response);
        this.subject.name = null;
        this.common.openSnackBar(`${response.name} added successfully!`);
      }, error => {
        this.common.hideProgressBar();
        this.common.openSnackBar(`Oops! Some error occurred`);
      });
  }

  updateSubject(row) {
    this.common.showProgressBar();
    this.api.patch('subjects/' + row.id, row)
      .map(res => res.json())
      .subscribe((response) => {
        this.fetchSubjects();
        this.common.hideProgressBar();
        this.common.openSnackBar(`Subject name updated successfully!`);
      }, error => {
        this.common.hideProgressBar();
        this.common.openSnackBar(`Unable to update subject!`);
      });
  }
}
