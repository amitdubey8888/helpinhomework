/**
 * Created by shubhamjaiswal on 29/07/17.
 */
export interface SubjectModel {
  name: "string",
  id: "string"
}
