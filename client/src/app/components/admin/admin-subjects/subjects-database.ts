/**
 * Created by shubhamjaiswal on 29/07/17.
 */
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {SubjectModel} from "./subject-model";

/** An example database that the data source uses to retrieve data for the table. */
export class SubjectsDatabase {
  /** Stream that emits whenever the data has been modified. */
  dataChange: BehaviorSubject<SubjectModel[]> = new BehaviorSubject<SubjectModel[]>([]);

  get data(): SubjectModel[] {
    return this.dataChange.value;
  }

  constructor() {
  }

  /** Adds a new subject to the database. */
  addSubject(subject) {
    const copiedData = this.data.slice();
    copiedData.push(SubjectsDatabase.createNewSubject(subject));
    this.dataChange.next(copiedData);
  }

  deleteSubject(subject) {
    let copiedData = this.data.slice();
    // copiedData.push(SubjectsDatabase.createNewSubject(subject));
    copiedData = copiedData.filter(psubject => {
      return psubject.id != subject.id
    });
    this.dataChange.next(copiedData);
  }

  deleteAllSubjects() {
    this.dataChange.next([]);
  }

  /** Builds and returns a new Subject. */
  private static createNewSubject(subject) {
    return {
      id: subject.id,
      name: subject.name,
    };
  }
}
