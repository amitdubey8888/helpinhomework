import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {ApiService} from '../../../providers/api.service';
import {CommonService} from '../../../providers/common.service';
import {MdPaginator, MdSort} from "@angular/material";
import {StudentsDatabase} from "./students-database";
import {StudentDataSource} from "./students-datasource";
import {Observable} from "rxjs/Observable";

@Component({
  selector: 'app-admin-student',
  templateUrl: './admin-students.component.html',
  styleUrls: ['./admin-students.component.css']
})
export class AdminStudentComponent implements OnInit {

  private activeTabIndex: number;
  displayedColumns = ['userName', 'email', 'joined', 'action'];
  private studentTabs = [
    {
      label: "ACTIVE",
      status: 'unprivileged'
    },
    {
      label: "DISABLED",
      status: 'disabled'
    }
  ];
  studentsDatabase = new StudentsDatabase();
  dataSource: StudentDataSource | null;

  @ViewChild(MdSort) sort: MdSort;
  @ViewChild(MdPaginator) paginator: MdPaginator;
  @ViewChild('filter') filter: ElementRef;

  constructor(public api: ApiService,
              public router: Router,
              public common: CommonService) {
  }

  ngOnInit() {
    this.activeTabIndex = 0;
    this.fetchStudents();
    this.dataSource = new StudentDataSource(this.studentsDatabase, this.paginator, this.sort);
    Observable.fromEvent(this.filter.nativeElement, 'keyup')
      .debounceTime(150)
      .distinctUntilChanged()
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      });
  }

  fetchStudents() {
    this.common.showProgressBar();
    this.api.get(`users?filter[where][and][0][realm]=student&filter[where][and][1][status]=${this.studentTabs[this.activeTabIndex].status}`)
      .map(res => res.json())
      .subscribe(students => {
          this.common.hideProgressBar();
          this.studentsDatabase.deleteAllUsers();
          students.forEach(student => this.studentsDatabase.addUser(student))
        },
        error => {
          this.common.hideProgressBar();
          this.common.openSnackBar('Error while fetching students')
        })
  }

  deleteStudent(student, list) {
    this.common.showProgressBar();
    this.api.delete('users/' + student.id)
      .map(res => res.json())
      .subscribe(
        (response) => {
          this.common.hideProgressBar();
          this.studentsDatabase.deleteUser(student);
          this.common.openSnackBar(`${student.name} Deleted`);
        }, error => {
          this.common.hideProgressBar();
          this.common.openSnackBar(`Unable to delete ${student.name}`);
          console.log('Unable to delete');
        });
  }

  disableStudent(student, days) {
    this.common.showProgressBar();
    student.status = 'disabled';
    student.optional = {
      disabledDays: days
    };
    this.api.put("users/updateUser", {details: student})
      .map(res => res.json())
      .subscribe(
        success => {
          this.common.hideProgressBar();
          this.studentsDatabase.deleteUser(student);
          this.common.openSnackBar(`Account disabled for ${student.name}!`);
        },
        error => {
          this.common.hideProgressBar();
          let text = JSON.parse(error._body);
          this.common.openSnackBar(`An error occurred, ${text.error.message}`);
        });
  }

  viewStudent(student) {
    console.log(student);
  }

  enableStudent(student) {
    this.common.showProgressBar();
    student.status = 'unprivileged';
    this.api.put("users/updateUser", {details: student})
      .map(res => res.json())
      .subscribe(
        success => {
          this.common.hideProgressBar();
          this.studentsDatabase.deleteUser(student);
          this.common.openSnackBar(`Account enabled for ${student.name}!`);
        },
        error => {
          this.common.hideProgressBar();
          let text = JSON.parse(error._body);
          this.common.openSnackBar(`An error occurred, ${text.error.message}`);
        });
  }
}
