/**
 * Created by shubhamjaiswal on 29/07/17.
 */
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {StudentModel} from "./student-model";

/** An example database that the data source uses to retrieve data for the table. */
export class StudentsDatabase {
  /** Stream that emits whenever the data has been modified. */
  dataChange: BehaviorSubject<StudentModel[]> = new BehaviorSubject<StudentModel[]>([]);

  get data(): StudentModel[] {
    return this.dataChange.value;
  }

  constructor() {
  }

  /** Adds a new user to the database. */
  addUser(student) {
    const copiedData = this.data.slice();
    copiedData.push(StudentsDatabase.createNewUser(student));
    this.dataChange.next(copiedData);
  }

  deleteUser(student) {
    let copiedData = this.data.slice();
    // copiedData.push(StudentsDatabase.createNewUser(student));
    copiedData = copiedData.filter(pstudent => {
      return pstudent.id != student.id
    });
    this.dataChange.next(copiedData);
  }

  deleteAllUsers() {
    this.dataChange.next([]);
  }

  /** Builds and returns a new User. */
  private static createNewUser(student) {
    return {
      id: student.id,
      name: `${student.first_name} ${student.last_name}`,
      email: student.email,
      profile_picture: student.profile_picture,
      username: student.username,
      createdAt: student.createdAt,
      status: student.status
    };
  }
}
