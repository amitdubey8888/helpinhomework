/**
 * Created by shubhamjaiswal on 29/07/17.
 */
export interface StudentModel {
  id: string;
  name: string;
  email: string;
  profile_picture: string;
  username: string;
  createdAt: string;
  status: string;
}
