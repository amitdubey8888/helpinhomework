/**
 * Created by shubhamjaiswal on 01/08/17.
 */
export interface QuestionModel {
  title: string,
  description: string,
  deadline: string,
  attachments: any[],
  bids: any[],
  invitations: any[],
  price: number,
  status: string,
  archived: boolean,
  optional: Object,
  createdAt: string,
  id: string,
  subjectId: string,
  userId: string,
  subject: {
    name: string,
  },
  extras: any[]
}
