import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ApiService} from "../../../providers/api.service";
import {CommonService} from "../../../providers/common.service";
import {MdPaginator, MdSort} from "@angular/material";
import {QuestionsDatabase} from "./questions-database";
import {QuestionDataSource} from "./questions-datasource";
import {Observable} from "rxjs/Observable";

@Component({
  selector: 'app-admin-questions',
  templateUrl: './admin-questions.component.html',
  styleUrls: ['./admin-questions.component.css']
})
export class AdminQuestionsComponent implements OnInit {

  private activeTabIndex: number;
  displayedColumns = ['id', 'title', 'action'];
  private questionTabs = [
    {
      label: "UNANSWERED",
      status: 'unanswered'
    },
    {
      label: "ANSWERED",
      status: 'answered'
    },
    {
      label: "CANCELLED",
      status: 'cancelled'
    },
    {
      label: "DENIED",
      status: 'denied'
    },
    {
      label: "IN PROGRESS",
      status: 'in-progress'
    },
    {
      label: "PAYMENT INCOMPLETE",
      status: 'payment-incomplete'
    },
    {
      label: "ACCEPTED",
      status: 'accepted'
    }
  ];
  questionsDatabase = new QuestionsDatabase();
  dataSource: QuestionDataSource | null;

  @ViewChild(MdSort) sort: MdSort;
  @ViewChild(MdPaginator) paginator: MdPaginator;
  @ViewChild('filter') filter: ElementRef;

  constructor(private api: ApiService,
              private common: CommonService) {
  }

  ngOnInit() {
    this.fetchQuestions();
    this.dataSource = new QuestionDataSource(this.questionsDatabase, this.paginator, this.sort);
    Observable.fromEvent(this.filter.nativeElement, 'keyup')
      .debounceTime(150)
      .distinctUntilChanged()
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      });
  }

  fetchQuestions() {
    this.common.showProgressBar();
    this.api.get('questions?filter[where][archived]=false&filter[include]=subject')
      .map(res => res.json())
      .subscribe((questions) => {
        this.common.hideProgressBar();
        this.questionsDatabase.deleteAllQuestions();
        questions.forEach(question => {
          if (question.subject) {
            this.questionsDatabase.addQuestion(question)
          }
        });
      }, error => {
        this.common.hideProgressBar();
        // this.common.openSnackBar(`Unable to fetch subjects`);
      })
  }

  deleteQuestion(question) {
    // swal({
    //   title: 'Are you sure?',
    //   text: 'You will not be able to undo this operation!',
    //   type: 'warning',
    //   showCancelButton: true,
    //   confirmButtonText: 'Yes, delete it!',
    //   cancelButtonText: 'No, keep it'
    // }).then(success => {
      this.common.showProgressBar();
      question.archived = true;
      this.api.patch('questions/' + question.id, question)
        .map(res => res.json())
        .subscribe((questions) => {
          this.common.hideProgressBar();
          this.fetchQuestions();
          // this.common.openSnackBar(`Deleted`);
        }, error => {
          this.common.hideProgressBar();
          // this.common.openSnackBar(`Unable to delete question. Please check your connection!`)
        });
    // }, dismiss => {
    // });
  }

  viewQuestion(question) {
    console.info('question', question)
  }
}
