/**
 * Created by shubhamjaiswal on 01/08/17.
 */
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {QuestionModel} from "./question-model";

/** An example database that the data source uses to retrieve data for the table. */
export class QuestionsDatabase {
  /** Stream that emits whenever the data has been modified. */
  dataChange: BehaviorSubject<QuestionModel[]> = new BehaviorSubject<QuestionModel[]>([]);

  get data(): QuestionModel[] {
    return this.dataChange.value;
  }

  constructor() {
  }

  /** Adds a new user to the database. */
  addQuestion(question) {
    const copiedData = this.data.slice();
    copiedData.push(QuestionsDatabase.createNewQuestion(question));
    this.dataChange.next(copiedData);
  }

  deleteQuestion(question) {
    let copiedData = this.data.slice();
    copiedData = copiedData.filter(pquestion => {
      return pquestion.id != question.id
    });
    this.dataChange.next(copiedData);
  }

  deleteAllQuestions() {
    this.dataChange.next([]);
  }

  /** Builds and returns a new Question. */
  private static createNewQuestion(question) {
    return {
      title: question.title,
      description: question.description,
      deadline: question.deadline,
      attachments: [],
      bids: [],
      invitations: [],
      price: 5,
      status: question.status,
      archived: false,
      optional: {},
      createdAt: question.createdAt,
      id: question.id,
      subjectId: question.subjectId,
      userId: question.userId,
      subject: {
        name: question.subject.name,
      },
      extras:[]
    };
  }
}
