import {NgModule} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";

import {AdminHomeComponent} from "./admin-home/admin-home.component";
import {AdminMenuComponent} from "./admin-menu/admin-menu.component";
import {AdminPaymentsComponent} from "./admin-payments/admin-payments.component";
import {AdminProfileComponent} from "./admin-profile/admin-profile.component";
import {AdminStudentComponent} from "./admin-students/admin-students.component";
import {AdminSubjectsComponent} from "./admin-subjects/admin-subjects.component";
import {AdminTutorComponent} from "./admin-tutors/admin-tutors.component";
import {AdminGuard} from "../../guards/admin.guard";
import {AdminQuestionsComponent} from "./admin-questions/admin-questions.component";

const adminRoutes:Routes = [{
  path: 'admin', component: AdminMenuComponent,
  children: [
    {path: 'home', component: AdminHomeComponent, canActivate: [AdminGuard]},
    {path: 'menu', component: AdminMenuComponent, canActivate: [AdminGuard]},
    {path: 'payments', component: AdminPaymentsComponent, canActivate: [AdminGuard]},
    {path: 'profile', component: AdminProfileComponent, canActivate: [AdminGuard]},
    {path: 'students', component: AdminStudentComponent, canActivate: [AdminGuard]},
    {path: 'subjects', component: AdminSubjectsComponent, canActivate: [AdminGuard]},
    {path: 'tutors', component: AdminTutorComponent, canActivate: [AdminGuard]},
    {path: 'questions', component: AdminQuestionsComponent, canActivate: [AdminGuard]},
    {path: '', redirectTo: '/admin/home', pathMatch: 'full'},
    {path: '**', redirectTo: '/admin/home'}
  ]
}];

@NgModule({
  imports: [
    RouterModule.forChild(adminRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AdminRoutingModule {
}
