import {Component, OnInit} from '@angular/core';
import {ApiService} from "../../../providers/api.service";
import {CommonService} from "../../../providers/common.service";

@Component({
  selector: 'app-admin-payments',
  templateUrl: './admin-payments.component.html',
  styleUrls: ['./admin-payments.component.css']
})
export class AdminPaymentsComponent implements OnInit {

  private transactions = [];

  constructor(private api:ApiService,
              private common:CommonService) {
  }

  ngOnInit() {
    this.getPayments();
  }

  getPayments() {
    this.common.showProgressBar();
    this.api.get(`payments?filter[order]=timestamp%20DESC&filter[include]=question&filter[include]=user`)
      .map(res => res.json())
      .subscribe(
        (response) => {
          this.common.hideProgressBar();
          this.transactions = response;
        },
        error => {
          this.common.openSnackBar(`Unable to fetch transactions. Please check your connection!`);
        })
  }

  refund(transaction) {
    // swal({
    //   title: 'Are you sure?',
    //   text: `$ ${transaction.paid} will be refunded to ${transaction.user.first_name}`,
    //   type: 'warning',
    //   showCancelButton: true,
    //   confirmButtonText: `Yes, I'm sure!`,
    //   cancelButtonText: 'No, let it be'
    // }).then(success => {
      this.common.openSnackBar(`$ ${transaction.paid} will be refunded to ${transaction.user.first_name} shortly!`);
    // }, error => {
    // });
  }
}
