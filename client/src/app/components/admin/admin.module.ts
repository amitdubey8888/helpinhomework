import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {AdminRoutingModule} from "./admin-routing.module";
import {AdminHomeComponent} from "./admin-home/admin-home.component";
import {AdminPaymentsComponent} from "./admin-payments/admin-payments.component";
import {AdminProfileComponent} from "./admin-profile/admin-profile.component";
import {AdminStudentComponent} from "./admin-students/admin-students.component";
import {AdminSubjectsComponent} from "./admin-subjects/admin-subjects.component";
import {AdminTutorComponent} from "./admin-tutors/admin-tutors.component";
import {AdminMenuComponent} from './admin-menu/admin-menu.component';
import {AdminQuestionsComponent} from './admin-questions/admin-questions.component';
import {TutorModule} from "../tutor/tutor.module";
import {MaterialModule} from "../../material/material.module";

@NgModule({
  imports: [
    CommonModule,
    AdminRoutingModule,
    FormsModule,
    TutorModule,
    MaterialModule
  ],
  declarations: [
    AdminHomeComponent,
    AdminPaymentsComponent,
    AdminProfileComponent,
    AdminStudentComponent,
    AdminSubjectsComponent,
    AdminTutorComponent,
    AdminMenuComponent,
    AdminQuestionsComponent
  ]
})
export class AdminModule {
}
