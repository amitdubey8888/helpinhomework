import {Component, OnInit} from '@angular/core';
import {ApiService} from "../../../providers/api.service";
import {CurrentUserService} from "../../../providers/current-user.service";
import {AccountService} from "../../../providers/account.service";
import {Router} from "@angular/router";
import {CommonService} from "../../../providers/common.service";

@Component({
  selector: 'app-admin-profile',
  templateUrl: './admin-profile.component.html',
  styleUrls: ['./admin-profile.component.css']
})
export class AdminProfileComponent implements OnInit {

  private editFlag:boolean = false;
  private newProfileImage:any;
  private newImgLink:any;
  private today:string = new Date().toString();
  private minDate:string = new Date("January 1, 1990 12:00:00").toString();
  private user = {
    "first_name": "",
    "last_name": "",
    "profile_picture": "",
    "gender": "",
    "dob": "",
    "phone": "",
    "alternate_email": "",
    "username": "",
    "email": ""
  };

  constructor(public api:ApiService,
              private currentUser:CurrentUserService,
              private account:AccountService,
              public router:Router,
              public common:CommonService) {
  }

  ngOnInit() {
    this.user = this.currentUser.info;
  }

  save(form) {
    this.user.username = this.user.username.toLowerCase();
    this.common.showProgressBar();
    this.account.changeAttributes(this.user)
      .map(res => res.json())
      .subscribe(
        success => {
          this.common.hideProgressBar();
          this.currentUser.info = this.user;
          this.editFlag = false;
          this.common.openSnackBar('account updated successfully');
        },
        error => {
          this.common.hideProgressBar();
          let text = JSON.parse(error._body);
          this.common.openSnackBar(`Unable to update, ${text.error.message}`);
        });
  }

  parseDate(dateString:string):Date {
    if (dateString) {
      return new Date(dateString);
    } else {
      return null;
    }
  }

  private attachmentsSelect(event) {
    this.newProfileImage = event.srcElement.files[0];
    // console.log(this.newProfileImage);
  }
}
