import {Component, Inject, OnInit} from '@angular/core';
import {MD_DIALOG_DATA, MdDialog, MdDialogRef} from "@angular/material";
import {ApiService} from "../../../providers/api.service";
import {AccountService} from "../../../providers/account.service";
import {CurrentUserService} from "../../../providers/current-user.service";
import {CommonService} from "../../../providers/common.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-view-user',
  templateUrl: './view-user.component.html',
  styleUrls: ['./view-user.component.css']
})
export class ViewUserComponent implements OnInit {

  private user: any = {
    "first_name": "",
    "last_name": "",
    "phone": "",
    "status": "requested",
    "archived": false,
    "realm": "tutor",
    "email": "",
    "password": "",
    "optional": {
      profileLink1: '',
      profileLink2: '',
      profileLink3: '',
      file1: null,
      file2: null,
      subjectsOfExpertise: [],
      whyHireMe: null
    },
    "extras": []
  };

  constructor(@Inject(MD_DIALOG_DATA) public data: any,
              public dialog: MdDialog,
              public dialogRef: MdDialogRef<ViewUserComponent>,
              private api: ApiService,
              private account: AccountService,
              private currentUser: CurrentUserService,
              private common: CommonService,
              private router: Router) {
  }

  ngOnInit() {
    this.user = Object.assign(this.user, this.data);
    this.fetchUserDetails()
  }

  fetchUserDetails() {
    // this.common.openSnackBar(`${this.user.realm} account created successfully`);
    // this.dialogRef.close();
  }
}
