import {Component, Inject, OnInit} from '@angular/core';
import {MD_DIALOG_DATA, MdDialog, MdDialogRef} from "@angular/material";
import {ApiService} from "../../../providers/api.service";
import {AccountService} from "../../../providers/account.service";
import {CurrentUserService} from "../../../providers/current-user.service";
import {CommonService} from "../../../providers/common.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-invite-tutor',
  templateUrl: './invite-tutor.component.html',
  styleUrls: ['./invite-tutor.component.css']
})

export class InviteTutorComponent implements OnInit {

  tutors = [];
  filteredTutors = [];

  constructor(@Inject(MD_DIALOG_DATA) public question: any,
              public dialog: MdDialog,
              public dialogRef: MdDialogRef<InviteTutorComponent>,
              private api: ApiService,
              private common: CommonService) {
  }

  ngOnInit() {
    this.fetchTutors()
  }

  fetchTutors() {
    this.common.showProgressBar();
    this.api.get(`users?filter[where][and][0][realm]=tutor&filter[where][and][1][status]=privileged`)
      .map(res => res.json())
      .subscribe(tutors => {
          this.common.hideProgressBar();
          tutors.forEach(tutor => {
            this.question.invitations.forEach(invitedTutor => {
              if(tutor.id == invitedTutor.id){
                tutor.invited = true;
                return
              }
            })
          });
          this.tutors = tutors;
          this.filteredTutors = tutors;
        },
        error => {
          this.common.hideProgressBar();
          this.common.openSnackBar('Error while fetching pending tutors')
        })
  }

  filterTutors(query?: any) {
    let params = {
      username: query
    };
    if (!query) {
      this.filteredTutors = this.tutors.slice(0);
    }

    this.filteredTutors = this.tutors.filter((item) => {
      for (let key in params) {
        let field = item[key];
        if (typeof field === 'string' && field.toLowerCase().indexOf(params[key].toLowerCase()) >= 0) {
          return item;
        } else if (field == params[key]) {
          return item;
        }
      }
      return null;
    });
  }

  toggleSelectedTutor(tutor) {
    this.tutors[this.tutors.findIndex(ltutor => tutor.id === ltutor.id)].selected = !this.tutors[this.tutors.findIndex(ltutor => tutor.id === ltutor.id)].selected
  }

  inviteSelectedTutors() {
    this.tutors.forEach(tutor => {
      if (tutor.selected) {
        this.question.invitations.push(tutor)
      }
    });
    this.common.showProgressBar();
    this.api.patch('questions/' + this.question.id, this.question)
      .map(res => res.json())
      .subscribe((response) => {
        this.common.hideProgressBar();
        this.common.openSnackBar(`Selected Tutors have been invited`);
        this.dialogRef.close(this.question);
      }, error => {
        this.common.hideProgressBar();
        this.common.openSnackBar(`Please check your connection and try again.!`);
      });
  }
}
