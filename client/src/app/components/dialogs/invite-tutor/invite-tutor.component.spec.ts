import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InviteTutorComponent } from './invite-tutor.component';

describe('InviteTutorComponent', () => {
  let component: InviteTutorComponent;
  let fixture: ComponentFixture<InviteTutorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InviteTutorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InviteTutorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
