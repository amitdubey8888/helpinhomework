import {MdDialogRef} from '@angular/material';
import {Component} from '@angular/core';
@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.css']
})
export class ConfirmComponent {
  public title: string;
  public message: string;

  constructor(public dialogRef: MdDialogRef<ConfirmComponent>) {

  }

}
