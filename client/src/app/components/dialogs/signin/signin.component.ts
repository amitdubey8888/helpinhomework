import {Component, Inject, OnInit} from '@angular/core';
import {ForgotPasswordComponent} from "../forgot-password/forgot-password.component";
import {MD_DIALOG_DATA, MdDialog, MdDialogRef} from "@angular/material";
import {ApiService} from "../../../providers/api.service";
import {AccountService} from "../../../providers/account.service";
import {CurrentUserService} from "../../../providers/current-user.service";
import {Router} from "@angular/router";
import {NgForm} from "@angular/forms";
import {CommonService} from "../../../providers/common.service";

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  private selected: string = 'Student';
  private showLogin: boolean = true;
  private user = {
    "first_name": "",
    "last_name": "",
    "status": "inactive",
    "archived": false,
    "realm": "",
    "username": "",
    "email": "",
    "password": ""
  };

  constructor(@Inject(MD_DIALOG_DATA) public data: any,
              public dialog: MdDialog,
              public dialogRef: MdDialogRef<SigninComponent>,
              private api: ApiService,
              private account: AccountService,
              private currentUser: CurrentUserService,
              private common: CommonService,
              private router: Router) {
  }

  ngOnInit() {
  }

  signup(form) {
    this.common.showProgressBar();
    this.user.username = this.user.username.toLowerCase();
    this.user.realm = this.selected.toLowerCase();
    this.user.status = 'unprivileged';
    this.api.post('users', this.user)
      .map(res => res.json())
      .subscribe(success => {
          this.common.hideProgressBar();
          this.currentUser.info = success.user;
          console.log('user', success.user);
          if (this.user.realm == 'student') {
            localStorage.setItem('studentToken', JSON.stringify(success.user));
            this.router.navigate(['/student']);
          }
          else if (this.user.realm == 'tutor') {
            localStorage.setItem('tutorToken', JSON.stringify(success.user));
            this.router.navigate(['/tutor']);
          } else {
            this.account.logout();
          }
          this.common.openSnackBar(`${this.user.realm} account created successfully`);
          this.dialogRef.close(this.data.nextRoute);
          form.resetForm();
        },
        error => {
          this.common.hideProgressBar();
          let text = JSON.parse(error._body);
          this.common.openSnackBar(`${this.user.realm} account could not be created, ${text.error.message}`);
        });
  }

  login(form: NgForm) {
    if (this.user.username.split('@').length != 1) {
      this.user.email = this.user.username;
      this.user.username = null;
    }
    this.common.showProgressBar();
    let credentials = {
      username: this.user.username.toLowerCase(),
      email: this.user.email,
      password: this.user.password,
      realm: this.selected.toLowerCase()
    };
    this.api.post('users/login', credentials)
      .map(res => res.json())
      .subscribe(success => {
          this.common.hideProgressBar();
          if (success.id)
            this.handleLoggedInUser(success);
          else
            this.common.openSnackBar(`Invalid Credentials!`);
          form.resetForm();
        },
        error => {
          this.common.hideProgressBar();
          this.common.openSnackBar(`Invalid Credentials!`);
        }
      );
  }

  handleLoggedInUser(token) {
    this.common.showProgressBar();
    this.account.getDetailsViaToken(token)
      .map(res => res.json())
      .subscribe((profile: any) => {
          this.common.hideProgressBar();
          if (profile.status == 'disabled') {
            this.common.openSnackBar(`Your account is pending for approval by admin.`);
          }
          else if (profile.archived) {
            this.common.openSnackBar(`Your account is currently deactivated, contact admin for details`);
          }
          else {
            this.currentUser.info = profile;
            if (profile.realm == 'tutor') {
              localStorage.setItem('tutorToken', JSON.stringify(profile));
              this.router.navigate(['/tutor']);
            }
            else {
              localStorage.setItem('studentToken', JSON.stringify(profile));
              this.router.navigate(['/student']);
            }
            if (profile.username)
              this.common.openSnackBar(`Logged in as ${profile.username}`);
            else
              this.common.openSnackBar(`Logged in`);
            this.dialogRef.close(this.data.nextRoute)
          }
        },
        error => {
          this.common.hideProgressBar();
          this.common.openSnackBar(`Oops! Something bad happened, retry`);
        })
  }

  openForgotDialog() {
    let forgotPassword = this.dialog.open(ForgotPasswordComponent, {
      height: '30vh',
      width: '60vw',
    });
  }
}
