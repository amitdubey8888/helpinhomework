import { Component, OnInit } from '@angular/core';
import {MdDialogRef, MdSnackBar} from "@angular/material";
import {AccountService} from "../../../providers/account.service";
import {NgForm} from "@angular/forms";
import {CommonService} from "../../../providers/common.service";

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  private loading: boolean = false;
  private resetEmail:string;

  constructor(public dialogRef: MdDialogRef<ForgotPasswordComponent>,
              public accountService: AccountService,
              public common:CommonService) { }

  ngOnInit() {
  }

  forgotPassword(form: NgForm) {
    this.common.showProgressBar();
    this.accountService.resetPassword(this.resetEmail)
      .map((response) => response.json())
      .subscribe(res => {
        this.common.hideProgressBar();
        this.common.openSnackBar(`Reset instructions sent to ${this.resetEmail}`);
        this.dialogRef.close(true);
        form.resetForm();
      }, error => {
        this.common.hideProgressBar();
        this.common.openSnackBar(`Unable to find ${this.resetEmail}! Are you registered?`);
      });
  }
}
