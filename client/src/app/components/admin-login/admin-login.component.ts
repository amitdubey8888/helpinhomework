import {Component, OnInit} from '@angular/core';
import {NgForm} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {ApiService} from "../../providers/api.service";
import {CurrentUserService} from "../../providers/current-user.service";
import {AccountService} from "../../providers/account.service";
import {CommonService} from "../../providers/common.service";

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.css']
})
export class AdminLoginComponent implements OnInit {

  private user = {
    "realm": "admin",
    "username": "",
    "email": "",
    "password": ""
  };
  private loading: boolean = false;

  constructor(private router: Router,
              private api: ApiService,
              private common: CommonService,
              private currentUser: CurrentUserService,
              private accountService: AccountService) {
  }

  ngOnInit() {
  }

  signin(form: NgForm) {
    this.common.showProgressBar();
    if (this.user.username.split('@').length != 1) {
      this.user.email = this.user.username;
      this.user.username = null;
    }
    this.api.post('users/login', this.user)
      .map(res => res.json())
      .subscribe(
        success => {
          this.common.hideProgressBar();
          if (success.id)
            this.handleLoggedInUser(success);
          else
            this.common.openSnackBar(`Invalid Credentials!`)
        },
        error => {
          this.common.hideProgressBar();
          this.common.openSnackBar(`Invalid Credentials!`)
        }
      );
  }

  handleLoggedInUser(token) {
    this.common.showProgressBar();
    this.accountService.getDetailsViaToken(token)
      .map(res => res.json())
      .subscribe(
        (profile: any) => {
          this.common.hideProgressBar();
          this.currentUser.info = profile;
          localStorage.setItem('adminToken', JSON.stringify(profile));
          this.router.navigate(['/admin']);
          if (profile.first_name)
            this.common.openSnackBar(`Logged in as ${profile.first_name}`);
          else
            this.common.openSnackBar(`Logged in`);
        },
        error => {
          this.common.hideProgressBar();
          this.common.openSnackBar(`Oops! Something bad happened, retry`);
        })
  }
}
