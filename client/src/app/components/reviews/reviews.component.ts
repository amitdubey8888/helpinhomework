import {Component, OnInit} from '@angular/core';
import {ApiService} from "../../providers/api.service";
import {CommonService} from "../../providers/common.service";

@Component({
  selector: 'app-reviews',
  templateUrl: './reviews.component.html',
  styleUrls: ['./reviews.component.css']
})
export class ReviewsComponent implements OnInit {

  private loading: boolean = false;
  private reviews = [];
  private skip: number = 0;
  private showMoreFlag: boolean = false;

  constructor(private api: ApiService,
              private common: CommonService) {
  }

  ngOnInit() {
    this.getReviews();
  }

  getReviews() {
    this.common.showProgressBar();
    this.api.get(`reviews?filter[order]=timestamp%20DESC&filter[limit]=10&filter[skip]=${this.skip}&filter[include]=user`)
      .map(res => res.json())
      .subscribe(response => {
        this.common.hideProgressBar();
        console.log(response);
        this.reviews = response;
        if (response.length < 10) {
          this.showMoreFlag = true;
        }
      }, error => {
        this.common.hideProgressBar();
        this.common.openSnackBar(`Oops some error occurred!`);
      })
  }

}
