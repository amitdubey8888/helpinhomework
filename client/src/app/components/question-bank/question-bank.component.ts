import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ApiService} from '../../providers/api.service';
import {AccountService} from "../../providers/account.service";
import {CurrentUserService} from "../../providers/current-user.service";
import {CommonService} from "../../providers/common.service";
import {ItemsService} from "../../providers/items.service";
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-question-bank',
  templateUrl: './question-bank.component.html',
  styleUrls: ['./question-bank.component.css']
})

export class QuestionBankComponent implements OnInit {

  private subjectsList = [];
  private questionsList = [];
  private loading: boolean = false;
  private visibleItems = [];
  private user: any;
  private password: string;
  private inputType: string = 'password';
  private searchFields = {
    question: '',
    subject: ''
  };
  private subject = ['C', 'C++', 'Java', 'Javascript', 'Angular', 'HTML', 'CSS'];
  private options = ['C', 'C++', 'Java', 'Javascript', 'Angular', 'HTML', 'CSS'];

  constructor(private api: ApiService,
              private account: AccountService,
              private items: ItemsService,
              private currentUser: CurrentUserService,
              private common: CommonService,
              private router: Router) {
  }

  ngOnInit() {
    this.getSubjects();
    this.getQuestions();
    this.user = this.currentUser.info;
  }

  getSubjects() {
    this.common.showProgressBar();
    this.api.get('subjects?filter[where][archived]=false')
      .map(res => res.json())
      .subscribe(
        (response) => {
          this.common.hideProgressBar();
          this.subjectsList = response;
        },
        error => {
          this.common.hideProgressBar();
          this.common.openSnackBar(`Unable to fetch subjects. Please check your connection!`);
        })
  }

  getQuestions() {
    this.common.showProgressBar();
    this.api.get(`questions?filter[where][and][0][archived]=false&filter[where][and][1][status]=completed&filter[include]=subject&filter[include]=user`)
      .map(res => res.json())
      .subscribe(response => {
        console.info("Questions:", response);
        this.common.hideProgressBar();
        this.visibleItems = response;
        this.questionsList = response;
      }, error => {
        this.common.hideProgressBar();
        this.common.openSnackBar(`Unable to fetch questions. Please check your connection!`);
      })
  }

  getItems() {
    console.log("Event", this.searchFields.question);
    let val = this.searchFields.question;
    if (!val || !val.trim()) {
      this.visibleItems = this.questionsList;
      return;
    }
    this.visibleItems = this.items.query({
      title: val
    });
    if (this.searchFields.subject) {
      this.visibleItems = this.visibleItems.filter(item => {
        return this.searchFields.subject == item.subjectId
      })
    }
  }

  seeAnswer(question) {
    console.log(question);
    this.common.questionDetails = question;
    this.router.navigateByUrl('/answer');
  }

  openChangePassword(content) {
    // this.openedModal = this.modalService.open(content);
  }

  closeChangePassword(form: NgForm) {
    form.resetForm();
    // this.openedModal.close();
  }

  changePassword(form: NgForm) {
    this.common.showProgressBar();
    this.user.password = this.password;
    this.account.changeAttributes(this.user)
      .map(res => res.json())
      .subscribe(
        success => {
          this.common.hideProgressBar();
          this.common.openSnackBar(`Password successfully changed`);
          this.closeChangePassword(form);
        },
        error => {
          this.common.hideProgressBar();
          let text = JSON.parse(error._body);
          this.common.openSnackBar(`Unable to change password, ${text.error.message}`);
        });
  }

  logout() {
    this.account.logout();
    this.user = null;
  }
}
