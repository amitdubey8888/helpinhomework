import {NgModule} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";
import {TutorMenuComponent} from "./tutor-menu/tutor-menu.component";
import {HomeComponent} from "../common/home/home.component";
import {TutorProfileComponent} from "./tutor-profile/tutor-profile.component";
import {TutorQnaComponent} from "./tutor-qna/tutor-qna.component";
import {TutorPaymentsComponent} from "./tutor-payments/tutor-payments.component";
import {MyAnswersComponent} from "./my-answers/my-answers.component";
import {QuestionComponent} from './question/question.component';
import {TutorGuard} from "../../guards/tutor.guard";
import {BecomeATutorComponent} from "../common/become-a-tutor/become-a-tutor.component";
import {TutorBrowseQuestionsComponent} from './tutor-browse-questions/tutor-browse-questions.component';

const tutorRoutes: Routes = [{
  path: 'tutor', component: TutorMenuComponent,
  children: [
    {path: 'home', component: HomeComponent, canActivate: [TutorGuard]},
    {path: 'profile', component: TutorProfileComponent, canActivate: [TutorGuard]},
    {path: 'browse-questions', component: TutorBrowseQuestionsComponent, canActivate: [TutorGuard]},
    {path: 'qna', component: TutorQnaComponent, canActivate: [TutorGuard]},
    {path: 'become-a-tutor', component: BecomeATutorComponent, canActivate: [TutorGuard]},
    {path: 'payments', component: TutorPaymentsComponent, canActivate: [TutorGuard]},
    {path: 'my-answers', component: MyAnswersComponent, canActivate: [TutorGuard]},
    {path: 'question', component: QuestionComponent, canActivate: [TutorGuard]},
    {path: '', redirectTo: '/tutor/home', pathMatch: 'full'},
    {path: '**', redirectTo: '/tutor/home'}
  ]
}];

@NgModule({
  imports: [
    RouterModule.forChild(tutorRoutes)
  ],
  exports: [
    RouterModule
  ]
})

export class TutorRoutingModule {
}
