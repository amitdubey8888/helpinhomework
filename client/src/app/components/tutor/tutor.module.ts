import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MyAnswersComponent} from './my-answers/my-answers.component';
import {TutorMenuComponent} from './tutor-menu/tutor-menu.component';
import {TutorQnaComponent} from './tutor-qna/tutor-qna.component';
import {TutorProfileComponent} from './tutor-profile/tutor-profile.component';
import {TutorPaymentsComponent} from './tutor-payments/tutor-payments.component';
import {TutorHomeComponent} from "./tutor-home/tutor-home.component";
import {TutorRoutingModule} from "./tutor-routing.module";
import {FormsModule} from "@angular/forms";
import {FroalaEditorModule, FroalaViewModule} from "angular-froala-wysiwyg";
import {SubjectFilterPipe} from "../../pipes/subject.pipe";
import {QuestionComponent} from './question/question.component';
import {MaterialModule} from "../../material/material.module";
import {FlexLayoutModule} from "@angular/flex-layout";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { TutorBrowseQuestionsComponent } from './tutor-browse-questions/tutor-browse-questions.component';
import { TutorAnswerComponent } from './tutor-answer/tutor-answer.component';

@NgModule({
  imports: [
    CommonModule,
    TutorRoutingModule,
    FormsModule,
    FroalaEditorModule.forRoot(),
    FroalaViewModule.forRoot(),
    FlexLayoutModule,
    BrowserAnimationsModule,
    MaterialModule
  ],
  declarations: [
    MyAnswersComponent,
    TutorMenuComponent,
    TutorHomeComponent,
    TutorQnaComponent,
    TutorProfileComponent,
    TutorPaymentsComponent,
    SubjectFilterPipe,
    QuestionComponent,
    TutorBrowseQuestionsComponent,
    TutorAnswerComponent
  ],
  exports: [
    SubjectFilterPipe
  ]
})
export class TutorModule {
}
