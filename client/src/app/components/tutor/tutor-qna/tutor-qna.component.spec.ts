import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TutorQnaComponent } from './tutor-qna.component';

describe('TutorQnaComponent', () => {
  let component: TutorQnaComponent;
  let fixture: ComponentFixture<TutorQnaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TutorQnaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TutorQnaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
