import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../../providers/api.service';
import {CurrentUserService} from '../../../providers/current-user.service';

import {NgForm} from "@angular/forms/forms";
import {CommonService} from "../../../providers/common.service";

@Component({
  selector: 'app-tutor-qna',
  templateUrl: './tutor-qna.component.html',
  styleUrls: ['./tutor-qna.component.css']
})
export class TutorQnaComponent implements OnInit {

  public questionsList = [];
  public addFlag = false;
  public loading: boolean = false;
  public subjects = [];
  public attachedFiles: any;
  private question = {
    "title": "",
    "description": "",
    "deadline": new Date(),
    "attachments": [],
    "invitations": [],
    "price": null,
    "subjectId": "",
    "userId": this.currentUser.info.id,
    "bids": [],
    "status": "completed",
    "optional": {
      selectedTutor: ''
    }
  };
  public answer = {
    "status": "",
    "description": "",
    "attachments": [],
    "preview": "",
    "optional": {},
    "extras": [],
    "questionId": "",
    "userId": ""
  };

  constructor(private api: ApiService,
              private common: CommonService,
              private currentUser: CurrentUserService,
              ) {
  }

  ngOnInit() {
    this.getQuestions();
  }

  getQuestions() {
    this.common.showProgressBar();
    this.api.get(`questions?filter[where][and][0][archived]=false&filter[where][and][1][userId]=${this.currentUser.info.id}&filter[include]=subject`)
      .map(res => res.json())
      .subscribe((response) => {
        this.common.hideProgressBar();
        this.questionsList = response;
      }, error => {
        this.common.hideProgressBar();
        this.common.openSnackBar(`Please check your connection and try again.`, `Unable to fetch questions!`)
      })
  }


  getSubjects() {
    this.common.showProgressBar();
    this.api.get('subjects?filter[where][archived]=false')
      .map(res => res.json())
      .subscribe(
        (response) => {
          this.common.hideProgressBar();
          this.subjects = response;
        },
        error => {
          this.common.hideProgressBar();
          this.common.openSnackBar(`Unable to fetch subjects, Please check your connection!`);
        })
  }

  private attachmentsSelect(event, flag) {
    this.attachedFiles = event.srcElement.files;
  }


  parseDate(dateString: string): Date {
    if (dateString) {
      return new Date(dateString);
    } else {
      return null;
    }
  }

  addQuestion() {
    console.log(this.question);
    this.common.showProgressBar();
    this.api.post('questions', this.question)
      .map(res => res.json())
      .subscribe((response) => {
        this.common.hideProgressBar();
        this.addAnswer(response.id);
      }, error => {
        this.common.hideProgressBar();
        this.common.openSnackBar(`Please check your connection and try again.`, `Unable to post question!`)
      });
  }

  addAnswer(id) {
    this.common.showProgressBar();
    this.answer.questionId = id;
    this.answer.status = "completed";
    this.answer.userId = this.currentUser.info.id;
    this.api.post('answers', this.answer)
      .map(res => res.json())
      .subscribe((response) => {
        this.common.hideProgressBar();
        this.common.openSnackBar(`Your question has been added to question bank`);
        this.addFlag = false;
        this.getQuestions();
      }, error => {
        this.addFlag = false;
        this.common.openSnackBar(`Please check your connection and try again.`, `Unable to add question!`)
      });
  }

  showPreview(qs) {
    this.getSubjects();
    this.question = qs;
    this.addFlag = true;
    this.showAnswer(qs.id);
  }

  showAnswer(id) {
    this.common.showProgressBar();
    this.api.get(`answers?filter[where][questionId]=${id}`)
      .map(res => res.json())
      .subscribe((response) => {
        this.common.hideProgressBar();
        this.answer = response[0];
      }, error => {
        this.common.hideProgressBar();
        this.common.openSnackBar(`Unable to fetch answer! Please check your internet connection`);
      })
  }

  clearData(form: NgForm) {
    this.question = {
      "title": "",
      "description": "",
      "deadline": new Date(),
      "attachments": [],
      "invitations": [],
      "price": null,
      "subjectId": "",
      "userId": this.currentUser.info.id,
      "bids": [],
      "status": "completed",
      "optional": {
        selectedTutor: ''
      }
    };
    this.answer = {
      "status": "",
      "description": "",
      "attachments": [],
      "preview": "",
      "optional": {},
      "extras": [],
      "questionId": "",
      "userId": ""
    };
  }

}
