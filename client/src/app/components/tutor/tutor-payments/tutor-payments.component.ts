import {Component, OnInit} from '@angular/core';
import {ApiService} from "../../../providers/api.service";

import {CurrentUserService} from "../../../providers/current-user.service";
import {CommonService} from "../../../providers/common.service";

@Component({
  selector: 'app-tutor-payments',
  templateUrl: './tutor-payments.component.html',
  styleUrls: ['./tutor-payments.component.css']
})
export class TutorPaymentsComponent implements OnInit {

  private loading: boolean = false;
  private transactions = [];

  constructor(private api: ApiService,
              private common: CommonService,
              private currentUser: CurrentUserService) {
  }

  ngOnInit() {
    this.getPayments();
  }

  getPayments() {
    this.common.showProgressBar();
    this.api.get(`payments?filter[order]=timestamp%20DESC&filter[include]=question&filter[include]=user`)
      .map(res => res.json())
      .subscribe(
        (response) => {
          this.common.hideProgressBar();
          this.transactions = response.filter(payment => {
            return payment.receiver == this.currentUser.info.id
          })
        },
        error => {
          this.common.openSnackBar(`Unable to fetch transactions. Please check your connection!`);
        })
  }

}
