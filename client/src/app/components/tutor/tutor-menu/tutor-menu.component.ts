import {Component, OnInit} from '@angular/core';
import {AccountService} from "../../../providers/account.service";
import {Router} from "@angular/router";
import {CurrentUserService} from "../../../providers/current-user.service";

import {NgForm} from "@angular/forms";

import {CommonService} from "../../../providers/common.service";

@Component({
  selector: 'app-tutor-menu',
  templateUrl: './tutor-menu.component.html',
  styleUrls: ['./tutor-menu.component.css']
})
export class TutorMenuComponent implements OnInit {

  private user:any;
  private openedModal:any;
  private inputType:string = 'password';
  private password:string;

  constructor(private account:AccountService,
              private currentUser:CurrentUserService,
              private router:Router,
              private common:CommonService) {
  }

  ngOnInit() {
    this.user = this.currentUser.info;
  }

  openChangePassword(content) {
    // this.openedModal = this.modalService.open(content);
  }

  closeChangePassword(form:NgForm) {
    form.resetForm();
    // this.openedModal.close();
  }

  changePassword(form:NgForm) {
    this.common.showProgressBar();
    this.user.password = this.password;
    this.account.changeAttributes(this.user)
      .map(res => res.json())
      .subscribe(
        success => {
          this.common.hideProgressBar();
          this.common.openSnackBar(`Password Changed`);
          this.closeChangePassword(form);
        },
        error => {
          this.common.hideProgressBar();
          let text = JSON.parse(error._body);
          this.common.openSnackBar(`Unable to change password, ${text.error.message}`);
        });
  }

  logout() {
    this.account.logout();
    this.router.navigate(['/home']);
  }
}
