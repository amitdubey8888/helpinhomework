import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {CommonService} from '../../../providers/common.service';
import {ApiService} from '../../../providers/api.service';
import {CurrentUserService} from '../../../providers/current-user.service';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {

  public questionDetails: any;
  public loading: boolean = false;
  public answerDetails: any;
  public paymentStatus = '';
  public price = 0;
  public payedAmount = 0;
  public openedModal: any;
  public reviewDetails = {
    comment: '',
    rating: 1
  };
  public tutorsList = [];
  public payFlag = false;
  public comments: any;
  public comment = '';
  public user: any;
  public paymentDetails = {
    cardNumber: '',
    expireDate: '',
    cvv: ''
  };
  public review = {
    text: '',
    rating: 1
  };

  private newQuestionsList = [];
  private privateQuestionsList = [];
  private bidsQuestionsList = [];
  private acceptedBidsQuestionsList = [];
  private subjectsList = [];
  private attachedFiles: any;
  private bidPrice: number = 0;
  private messages = [];
  private student: any;
  private message: string;
  private answer = {
    "status": "",
    "description": "",
    "attachments": [],
    "preview": "",
    "optional": {},
    "extras": [],
    "questionId": "",
    "userId": ""
  };


  constructor(private common: CommonService,
              private router: Router,
              private api: ApiService,
              private currentUser: CurrentUserService) {
  }

  ngOnInit() {
    this.user = this.currentUser.info;
    if (this.common.questionDetails !== null) {
      this.questionDetails = this.common.questionDetails;

      this.getAnswer();
      this.getComments();


    } else {
      this.router.navigateByUrl('/tutor/home');
    }
  }

  getAnswer() {
    this.common.showProgressBar();
    this.api.get(`answers?filter[where][questionId]=${this.questionDetails.id}`)
      .map(res => res.json())
      .subscribe((response) => {
        this.common.hideProgressBar();
        this.answerDetails = response[0];
        console.log("Answer", this.answerDetails);
      }, error => {
        this.common.hideProgressBar();
        this.common.openSnackBar(`Unable to fetch answer! Please check your internet connection`);
      })
  }

  giveAnswer(answerModal) {
    // this.openedModal = this.modalService.open(answerModal, { size: "lg" });
  }

  private attachmentsSelect(event, flag) {
    this.attachedFiles = event.srcElement.files;
  }

  saveAnswer() {
    this.openedModal.close();
    this.answer.status = "pending";
    this.answer.userId = this.currentUser.info.id;
    this.answer.questionId = this.questionDetails.id;
    this.common.showProgressBar();
    this.api.post('answers', this.answer)
      .map(res => res.json())
      .subscribe((response) => {
        this.common.hideProgressBar();
        this.updateQuestion();
      }, error => {
        this.common.hideProgressBar();
        this.common.openSnackBar(`Answered saved successfully`)
      });
  }

  updateQuestion() {
    this.common.showProgressBar();
    this.questionDetails.status = "answered";
    this.api.patch('questions/' + this.questionDetails.id, this.questionDetails)
      .map(res => res.json())
      .subscribe((response) => {
        this.common.hideProgressBar();
        this.getAnswer();
      }, error => {
        this.common.hideProgressBar();
        this.common.openSnackBar(`Some error occurred. Unable to update`);
      });
  }


  getComments() {
    this.common.showProgressBar();
    this.comments = [];
    this.api.get(`comments?filter[where][questionId]=${this.questionDetails.id}&filter[include]=user`)
      .map(res => res.json())
      .subscribe(response => {
        this.comments = response;
        this.common.hideProgressBar();
      }, error => {
        this.common.openSnackBar(`Opps some error occurred!`);
        this.common.hideProgressBar();
      })
  }

  postComment() {
    this.common.showProgressBar();
    let info = {
      message: this.comment,
      userId: this.currentUser.info.id,
      questionId: this.questionDetails.id
    };


    this.api.post(`comments`, info)
      .map(res => res.json())
      .subscribe(response => {
        this.common.hideProgressBar();
        console.log(response);
        this.comment = '';
        this.getComments();
      }, error => {
        this.common.openSnackBar(`Oops some error occurred!`);
        this.common.hideProgressBar();
      });
  }

}
