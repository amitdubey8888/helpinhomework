import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TutorBrowseQuestionsComponent } from './tutor-browse-questions.component';

describe('TutorBrowseQuestionsComponent', () => {
  let component: TutorBrowseQuestionsComponent;
  let fixture: ComponentFixture<TutorBrowseQuestionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TutorBrowseQuestionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TutorBrowseQuestionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
