/**
 * Created by shubhamjaiswal on 01/08/17.
 */
export interface QuestionModel {
  title: string,
  description?: string,
  deadline: string,
  attachments?: any[],
  invitations?: any[],
  price: number,
  status: string,
  archived?: boolean,
  createdAt: string,
  id: string,
  subjectId: string,
  userId: string,
  subject: Object,
  comments?: Object[],
  answers?: Object,
  bids?: Object[],
  user: Object
}
