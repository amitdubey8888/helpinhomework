import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {CurrentUserService} from "../../../providers/current-user.service";
import {QuestionsDatabase} from "./questions-database";
import {QuestionDataSource} from "./questions-datasource";
import {MdPaginator, MdSort} from "@angular/material";
import {ApiService} from "../../../providers/api.service";
import {CommonService} from "../../../providers/common.service";
import {Observable} from "rxjs/Observable";
import {DataService} from "../../../providers/data.service";

@Component({
  selector: 'app-tutor-browse-questions',
  templateUrl: './tutor-browse-questions.component.html',
  styleUrls: ['./tutor-browse-questions.component.css']
})
export class TutorBrowseQuestionsComponent implements OnInit {

  private profileLink: string;
  private activeTabIndex: number;
  private subjects = [];
  private selectedSubject;
  private showAssignedTutors: boolean;
  private displayedColumns = ['question', 'replies', 'tutor', 'dateAsked', 'timeLeft', 'value'];
  private text: any = {
    "Weeks": "Weeks",
    "Days": "Days", "Hours": "Hours",
    Minutes: "Minutes", "Seconds": "Seconds",
    "MilliSeconds": "MilliSeconds"
  };
  private questionTabs = [
    {
      label: "NEW QUESTIONS",
      status: 'unanswered'
    },
    {
      label: "PRIVATE QUESTIONS",
      status: 'answered'
    },
    {
      label: "PLACED BIDS",
      status: 'cancelled'
    },
    {
      label: "MY QUESTIONS",
      status: 'denied'
    }
  ];
  questionsDatabase = new QuestionsDatabase();
  dataSource: QuestionDataSource | null;

  @ViewChild(MdSort) sort: MdSort;
  @ViewChild(MdPaginator) paginator: MdPaginator;
  @ViewChild('filter') filter: ElementRef;

  constructor(private currentUser: CurrentUserService,
              private api: ApiService,
              private data: DataService,
              private common: CommonService) {
  }

  ngOnInit() {
    this.getSubjects();
    this.activeTabIndex = 0;
    this.selectedSubject = 'all';
    this.profileLink = `https://www.helpinhomework.org/${this.currentUser.info.username}`;
    this.fetchQuestions();
    this.dataSource = new QuestionDataSource(this.questionsDatabase, this.paginator, this.sort);
    Observable.fromEvent(this.filter.nativeElement, 'keyup')
      .debounceTime(150)
      .distinctUntilChanged()
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      });
  }

  subjectChanged() {
    console.log('sel. assigned', this.showAssignedTutors)
  }

  getSubjects() {
    this.common.showProgressBar();
    this.api.get('subjects?filter[where][archived]=false')
      .map(res => res.json())
      .subscribe(response => {
          this.common.hideProgressBar();
          this.subjects = response;
        },
        error => {
          this.common.hideProgressBar();
          this.common.openSnackBar(`Unable to fetch subjects, Please check your connection!`);
        })
  }

  fetchQuestions() {
    this.common.showProgressBar();
    this.data.fetchAllQuestions(this.selectedSubject)
      .subscribe((questions) => {
        this.common.hideProgressBar();
        this.questionsDatabase.deleteAllQuestions();
        this.processQuestions(questions);
        console.log(`Questions`, questions)
      }, error => {
        this.common.hideProgressBar();
        this.common.openSnackBar(`Unable to fetch subjects`);
      })
  }

  deleteQuestion(question) {
    //_todo show confirm dialog here
    // swal({
    //   title: 'Are you sure?',
    //   text: 'You will not be able to undo this operation!',
    //   type: 'warning',
    //   showCancelButton: true,
    //   confirmButtonText: 'Yes, delete it!',
    //   cancelButtonText: 'No, keep it'
    // }).then(success => {
    this.common.showProgressBar();
    question.archived = true;
    this.api.patch('questions/' + question.id, question)
      .map(res => res.json())
      .subscribe((questions) => {
        this.common.hideProgressBar();
        this.fetchQuestions();
        this.common.openSnackBar(`Deleted`);
      }, error => {
        this.common.hideProgressBar();
        this.common.openSnackBar(`Unable to delete question. Please check your connection!`)
      });
    // }, dismiss => {
    // });
  }

  processQuestions(questions) {
    switch (this.activeTabIndex) {
      case 0:
        this.displayedColumns = ['question', 'replies', 'tutor', 'dateAsked', 'timeLeft', 'value'];
        questions.forEach(question => {
          if (!this.userIsInvited(question) && !this.userHasBid(question)) {
            this.questionsDatabase.addQuestion(question);
          }
        });
        break;
      case 1:
        this.displayedColumns = ['question', 'replies', 'tutor', 'dateAsked', 'timeLeft', 'value'];
        questions.forEach(question => {
          if (this.userIsInvited(question) && !this.userHasBid(question)) {
            this.questionsDatabase.addQuestion(question);
          }
        });
        break;
      case 2:
        this.displayedColumns = ['question', 'replies', 'tutor', 'dateAsked', 'timeLeft', 'value', 'bidDeadline', 'bidValue'];
        questions.forEach(question => {
          question.bids.forEach(bid => {
            if (this.currentUser.info.id === bid.userId && !bid.bid_selected) {
              this.questionsDatabase.addQuestion(question);
            }
          })
        });
        break;
      case 3:
        this.displayedColumns = ['question', 'replies', 'tutor', 'dateAsked', 'timeLeft', 'value', 'status', 'answerTime'];
        questions.forEach(question => {
          question.bids.forEach(bid => {
            if (this.currentUser.info.id === bid.userId && bid.bid_selected) {
              this.questionsDatabase.addQuestion(question);
            }
          })
        });
        break;
    }
  }

  userIsInvited(question): boolean {
    let invitation = false;
    question.invitations.forEach(tutor => {
      if (this.currentUser.info.id === tutor.id) {
        invitation = true;
        return;
      }
    });
    return invitation
  }

  userHasBid(question): boolean {
    let hasBid = false;
    question.bids.forEach(bid => {
      if (bid.userId === this.currentUser.info.id) {
        hasBid = true;
        return;
      }
    });
    return hasBid
  }
}
