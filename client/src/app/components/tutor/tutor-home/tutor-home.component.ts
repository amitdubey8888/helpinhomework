import {Component, OnInit} from '@angular/core';
// import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import {Router} from "@angular/router";
import {ApiService} from '../../../providers/api.service';
import {CurrentUserService} from '../../../providers/current-user.service';
import {AccountService} from '../../../providers/account.service';

import {CommonService} from "../../../providers/common.service";


@Component({
  selector: 'app-tutor-home',
  templateUrl: './tutor-home.component.html',
  styleUrls: ['./tutor-home.component.css'],
})

export class TutorHomeComponent implements OnInit {

  private newQuestionsList = [];
  private privateQuestionsList = [];
  private bidsQuestionsList = [];
  private acceptedBidsQuestionsList = [];
  private subjectsList = [];
  private openedModal: any;
  private loading: boolean = false;
  private attachedFiles: any;
  private bidPrice: number = 0;
  private messages = [];
  private student: any;
  private message: string;
  private answer = {
    "status": "",
    "description": "",
    "attachments": [],
    "preview": "",
    "optional": {},
    "extras": [],
    "questionId": "",
    "userId": ""
  };
  private selectedQuestion = {
    "title": "",
    "status": "",
    "id": ""
  };

  constructor(private api: ApiService,
              private currentUser: CurrentUserService,

              private common: CommonService,
              private router: Router) {
  }

  ngOnInit() {
    this.getSubjects();
    this.getNewQuestions();
  }

  getSubjects() {
    this.common.showProgressBar();
    this.api.get('subjects?filter[where][archived]=false')
      .map(res => res.json())
      .subscribe((response) => {
        this.subjectsList = response;
        this.common.hideProgressBar();
      }, error => {
        this.common.hideProgressBar();
        this.common.openSnackBar(`Unable to fetch subjects`);
      })
  }


  getNewQuestions() {
    this.common.showProgressBar();
    this.api.get(`questions?filter[where][and][0][archived]=false&filter[where][and][1][status]=&filter[include]=user&filter[include]=subject`)
      .map(res => res.json())
      .subscribe((response) => {
        this.common.hideProgressBar();
        this.newQuestionsList = [];
        this.privateQuestionsList = [];
        this.bidsQuestionsList = [];
        this.acceptedBidsQuestionsList = [];
        for (let qs of response) {
          let flag = false;
          if (qs.optional.selectedTutor === this.currentUser.info.id) {
            this.acceptedBidsQuestionsList.push(qs);
            flag = true;
          }

          if (flag != true) {
            for (let bid of qs.bids) {
              if (bid.userId === this.currentUser.info.id) {
                this.bidsQuestionsList.push(qs);
                flag = true;
              }
            }
          }

          if (flag != true && qs.invitations.indexOf(this.currentUser.info.email) !== -1) {
            this.privateQuestionsList.push(qs);
            flag = true;
          }

          if (flag != true) {
            this.newQuestionsList.push(qs);
          }
        }
      }, error => {
        this.common.hideProgressBar();
        this.common.openSnackBar(`Unable to fetch questions. Please check your connection!`)
      })
  }

  apply(qs) {
    this.common.showProgressBar();
    qs.bids.push({userId: this.currentUser.info.id, price: this.bidPrice});
    console.log(qs);
    this.api.patch('questions/' + qs.id, qs)
      .map(res => res.json())
      .subscribe((response) => {
        this.common.hideProgressBar();
        this.getNewQuestions();
      }, error => {
        this.common.hideProgressBar();
        this.common.openSnackBar(`Some error occurred. Unable to apply`)
      });
  }

  giveAnswer(question) {
    this.common.questionDetails = question;
    this.router.navigateByUrl('/tutor/question');
  }

  open(answerModal) {
    // this.openedModal = this.modalService.open(answerModal, { size: "lg" });
  }

  private attachmentsSelect(event, flag) {
    this.attachedFiles = event.srcElement.files;
  }

  saveAnswer() {
    this.openedModal.close();
    this.answer.status = "pending";
    this.answer.userId = this.currentUser.info.id;
    this.common.showProgressBar();
    this.api.post('answers', this.answer)
      .map(res => res.json())
      .subscribe((response) => {
        this.common.hideProgressBar();
        this.updateQuestion();
      }, error => {
        this.common.hideProgressBar();
        this.common.openSnackBar(`Answered saved successfully`)
      });
  }

  updateQuestion() {
    this.common.showProgressBar();
    this.selectedQuestion.status = "answered";
    this.api.patch('questions/' + this.answer.questionId, this.selectedQuestion)
      .map(res => res.json())
      .subscribe((response) => {
        this.common.hideProgressBar();
        this.router.navigateByUrl('/tutor/my-answers');
      }, error => {
        this.common.hideProgressBar();
        this.common.openSnackBar(`Some error occurred. Unable to update`);
      });
  }

  talk(qs, talkModal) {
    this.common.showProgressBar();
    this.selectedQuestion = qs;
    this.api.get(`users/${qs.userId}`)
      .map(res => res.json())
      .subscribe(response => {
        this.student = response;
        this.common.hideProgressBar();
      }, error => {
        this.common.openSnackBar(`Oops some error occurred!`);
        this.common.hideProgressBar();
      });
    this.api.get(`comments?filter[where][questionId]=${qs.id}`)
      .map(res => res.json())
      .subscribe(response => {
        this.messages = response;
        this.common.hideProgressBar();
      }, error => {
        this.common.openSnackBar(`Opps some error occurred!`);
        this.common.hideProgressBar();
      })
    // this.openedModal = this.modalService.open(talkModal);
  }

  send() {
    this.common.showProgressBar();
    let comment = {
      message: this.message,
      userId: this.currentUser.info.id,
      questionId: this.selectedQuestion.id
    };

    this.api.post(`comments`, comment)
      .map(res => res.json())
      .subscribe(response => {
        this.common.hideProgressBar();
        this.messages.push(comment);
        this.message = null;
      }, error => {
        this.common.openSnackBar(`Oops some error occurred!`);
        this.common.hideProgressBar();
      });
  }
}
