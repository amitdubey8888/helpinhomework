import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../../providers/api.service';
import {CurrentUserService} from '../../../providers/current-user.service';
import {CommonService} from "../../../providers/common.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-my-answers',
  templateUrl: './my-answers.component.html',
  styleUrls: ['./my-answers.component.css']
})
export class MyAnswersComponent implements OnInit {

  private pendingAnswers = [];
  private previewedAnswers = [];
  private completedAnswers = [];
  private loading: boolean;
  private subjectsList = [];
  private openedModal: any;
  private answerDetails: any;

  constructor(private api: ApiService,
              private currentUser: CurrentUserService,
              private common: CommonService,
              private router: Router) {
  }

  ngOnInit() {
    this.getSubjects();
    this.getAnswers();
  }

  getAnswers() {
    this.common.showProgressBar();
    this.api.get(`answers?filter[where][and][0][archived]=false&filter[where][and][1][userId]=${this.currentUser.info.id}&filter[include]=question`)
      .map(res => res.json())
      .subscribe((response) => {
        this.common.hideProgressBar();
        for (let qs of response) {
          if (qs.status === 'pending') {
            this.pendingAnswers.push(qs);
          } else if (qs.status === 'previewed') {
            this
              .previewedAnswers
              .push(qs);
          } else if (qs.status === 'completed') {
            this.completedAnswers.push(qs);
          }
        }
      }, error => {
        this.common.hideProgressBar();
        this.common.openSnackBar(`Unable to fetch answers`, `Please check your connection!`)
      })
  }

  getSubjects() {
    this.common.showProgressBar();
    this
      .api
      .get('subjects?filter[where][archived]=false')
      .map(res => res.json())
      .subscribe((response) => {
        this.common.hideProgressBar();
        this.subjectsList = response;
      }, error => {
        this.common.hideProgressBar();
        this.common.openSnackBar(`Unable to fetch subjects`, `Please check your connection!`)
      })
  }

  seeAnswer(ans) {
    this.common.showProgressBar();
    this.api.get(`questions?filter[where][id]=${ans.question.id}&filter[include]=subject`)
      .map(res => res.json())
      .subscribe((response) => {
        this.common.hideProgressBar();
        this.common.questionDetails = response[0];
        this.router.navigateByUrl('/tutor/question');
      }, error => {
        this.common.hideProgressBar();
        this.common.openSnackBar(`Unable to fetch subjects`, `Please check your connection!`)
      })

  }
}
