import {Component, OnInit} from '@angular/core';
import {ApiService} from "../../../providers/api.service";
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {

  private user: any = {
    email: null,
    message: null
  };

  constructor(private api: ApiService) {
  }

  ngOnInit() {
  }

  contactUs(form: NgForm) {
    this.api.post("users/sendMail", {details: this.user})
      .map(res => res.json())
      .subscribe(response => console.log('email res', response), error => console.error('mail error', error))
  }
}
