import 'hammerjs';
import {APP_BASE_HREF} from "@angular/common";
import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {FlexLayoutModule} from "@angular/flex-layout";
import {FroalaEditorModule, FroalaViewModule} from 'angular-froala-wysiwyg';
import {AppRoutingModule} from "./app.routing";
import {AdminModule} from "./components/admin/admin.module";
import {StudentModule} from "./components/student/student.module";
import {TutorModule} from "./components/tutor/tutor.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MaterialModule} from "./material/material.module";

import {ApiService} from "./providers/api.service";
import {CommonService} from './providers/common.service';
import {CurrentUserService} from "./providers/current-user.service";
import {AccountService} from "./providers/account.service";
import {ItemsService} from "./providers/items.service";
import {WindowRefService} from "./providers/window-ref.service";

import {TutorGuard} from "./guards/tutor.guard";
import {StudentGuard} from "./guards/student.guard";
import {AdminGuard} from "./guards/admin.guard";
import {HomeGuard} from "./guards/home.guard";
import {SignupGuard} from "./guards/signup.guard";
import {LoginGuard} from "./guards/login.guard";
import {QbankGuard} from "./guards/qbank.guard";
import {AdminLoginGuard} from "./guards/admin-login.guard";

import {AppComponent} from './app.component';
import {QuestionBankComponent} from './components/question-bank/question-bank.component';
import {HomeComponent} from './components/common/home/home.component';
import {AdminLoginComponent} from './components/admin-login/admin-login.component';
import {AnswerComponent} from './components/answer/answer.component';
import {ReviewsComponent} from './components/reviews/reviews.component';
import {AboutUsComponent} from './components/app-content/about-us/about-us.component';
import {HowItWorksComponent} from './components/app-content/how-it-works/how-it-works.component';
import {PricingComponent} from './components/app-content/pricing/pricing.component';
import {RefundPolicyComponent} from './components/app-content/refund-policy/refund-policy.component';
import {FaqComponent} from './components/app-content/faq/faq.component';
import {ContactUsComponent} from './components/app-content/contact-us/contact-us.component';
import {PrivacyPolicyComponent} from './components/app-content/privacy-policy/privacy-policy.component';
import {TermsAndConditionsComponent} from './components/app-content/terms-and-conditions/terms-and-conditions.component';
import {ConnectWithUsComponent} from './components/app-content/connect-with-us/connect-with-us.component';
import {BecomeATutorComponent} from './components/common/become-a-tutor/become-a-tutor.component';
import {SigninComponent} from './components/dialogs/signin/signin.component';
import {ForgotPasswordComponent} from './components/dialogs/forgot-password/forgot-password.component';
import {ProfileSettingsComponent} from './components/common/profile-settings/profile-settings.component';
import {ViewUserComponent} from './components/dialogs/view-user/view-user.component';
import {AskAQuestionComponent} from './components/common/ask-a-question/ask-a-question.component';
import {ConfirmComponent} from './components/dialogs/confirm/confirm.component';
import {DataService} from "./providers/data.service";
import {DialogService} from "./providers/dialog.service";
import {SigninService} from "./providers/signin.service";
import {CommonGuard} from "./guards/common.guard";
import {UploadAndEarnComponent} from './components/common/upload-and-earn/upload-and-earn.component';
import {InviteTutorComponent} from './components/dialogs/invite-tutor/invite-tutor.component';
import {MdDatetimepickerModule} from "md-datetimepicker";
import {MyAccountComponent} from './components/common/my-account/my-account.component';
import {ProfileComponent} from './components/common/profile/profile.component';
import {AvatarModule} from "ngx-avatar";

@NgModule({
  declarations: [
    AppComponent,
    QuestionBankComponent,
    HomeComponent,
    AdminLoginComponent,
    AnswerComponent,
    ReviewsComponent,
    AboutUsComponent,
    HowItWorksComponent,
    PricingComponent,
    RefundPolicyComponent,
    FaqComponent,
    ContactUsComponent,
    PrivacyPolicyComponent,
    TermsAndConditionsComponent,
    ConnectWithUsComponent,
    BecomeATutorComponent,
    SigninComponent,
    ForgotPasswordComponent,
    ProfileSettingsComponent,
    ViewUserComponent,
    AskAQuestionComponent,
    ConfirmComponent,
    UploadAndEarnComponent,
    InviteTutorComponent,
    MyAccountComponent,
    ProfileComponent,
  ],
  entryComponents: [
    ForgotPasswordComponent,
    SigninComponent,
    ViewUserComponent,
    InviteTutorComponent,
    ConfirmComponent
  ],
  imports: [
    AdminModule, StudentModule, TutorModule, BrowserModule,
    BrowserAnimationsModule, FormsModule, ReactiveFormsModule,
    HttpModule, AppRoutingModule, FlexLayoutModule,
    BrowserAnimationsModule, MaterialModule,
    MdDatetimepickerModule, AvatarModule,
    FroalaEditorModule.forRoot(),
    FroalaViewModule.forRoot()
  ],
  providers: [
    {provide: APP_BASE_HREF, useValue: '/'},
    ApiService, CommonService, DataService, DialogService,
    CurrentUserService, AccountService, ItemsService,
    WindowRefService, TutorGuard, StudentGuard,
    AdminGuard, HomeGuard, SignupGuard, CommonGuard,
    LoginGuard, QbankGuard, AdminLoginGuard, SigninService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
