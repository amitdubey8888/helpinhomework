var config = require('../../server/config.json');
var path = require('path');
var schedule = require('node-schedule');

module.exports = function (User) {
  // send verification email after registration
  User.afterRemote('create', function (context, user, next) {
    console.log('> user.afterRemote triggered');

    var options = {
      type: 'email',
      host: config.dev_ip,
      to: user.email,
      from: 'noreply@helpInHomework.com',
      subject: 'Thanks for registering.',
      template: path.resolve(__dirname, '../../server/views/verify.ejs'),
      redirect: '/verified',
      user: user
    };

    user.verify(options, function (err, response) {
      if (err) {
        User.deleteById(user.id);
        return next(err);
      }
      console.log('> verification email sent:', response);
      context.res.send({
        'info': 'SignUp Successful',
        'id': response.token,
        'userId': response.uid,
        'status': 'success',
        'user': user
      });
    });
  });

  //send password reset link when requested
  User.on('resetPasswordRequest', function (info) {
    var url = 'http://' + config.dev_ip + ':' + config.port + '/reset-password';
    var html = 'Click <a href="' + url + '?access_token=' +
      info.accessToken.id + '">here</a> to reset your password';

    User.app.models.Email.send({
      to: info.email,
      from: info.email,
      subject: 'Password reset',
      html: html
    }, function (err) {
      if (err) return console.log('> error sending password reset email');
      console.log('> sending password reset email to:', info.email);
    });
  });

  User.updateUser = function (details, cb) {
    var newErrMsg, newErr;
    if (!details.id) {
      newErrMsg = "Id is missing!";
      newErr = new Error(newErrMsg);
      newErr.statusCode = 404;
      newErr.code = 'USER_ID_MISSING';
      cb(newErr, false);
    }
    else {
      try {
        this.findOne({where: {'id': details.id}}, function (err, user) {
          if (err) {
            cb(err, false);
          } else if (!user) {
            newErrMsg = "User account does not exists!";
            newErr = new Error(newErrMsg);
            newErr.statusCode = 401;
            newErr.code = 'USER_NOT_FOUND';
            cb(newErr);
          } else {
            if (details.status === "disabled") {
              var disabledDays = 14;
              if (details.optional && details.optional.disabledDays)
                disabledDays = details.optional.disabledDays;
              var date = new Date(new Date().getTime() + disabledDays * 24 * 3600 * 1000);
              // var date = new Date(new Date().getTime() + 5000);
              schedule.scheduleJob(date, function () {
                details.status = "unprivileged";
                user.updateAttributes(details, function (err, instance) {
                  if (err) {
                    console.log("Error while updating in schedule", err)
                  } else {
                    console.log("Success while updating in schedule", instance)
                  }
                });
              });
            }
            //_todo user details already exist check
            user.updateAttributes(details, function (err, instance) {
              if (err) {
                cb(err);
              } else {
                cb(null, true);
              }
            });
          }
        })
      } catch (err) {
        cb(err);
      }
    }
  };

  User.sendMail = function (details, cb) {
    console.log('details', details);
    try {
      var html = '<p>' + details.message + '</p>';
      User.app.models.Email.send({
        to: details.to || 'support@helpinhomework.org',
        from: details.email || 'support@helpinhomework.org',
        subject: details.subject || 'Message from ' + details.email,
        html: html
      }, function (err) {
        if (err) {
          cb(err);
          return console.log('> error sending email');
        }
        cb(null, true);
        console.log('> email sent');
      });
    } catch (err) {
      cb(err);
    }
  };

  User.remoteMethod(
    'updateUser',
    {
      description: "Allows anyone to change a user details.",
      http: {verb: 'put'},
      accepts: [
        {arg: 'details', type: 'object', required: true, description: "User details to update"}
      ],
      returns: {arg: 'accountUpdated', type: 'boolean'}
    }
  );
  User.remoteMethod(
    'sendMail', {
      description: "Allows anyone to send a mail.",
      http: {verb: 'post'},
      accepts: [
        {arg: 'details', type: 'object', required: true, description: "email details"}
      ],
      returns: {arg: 'delivered', type: 'boolean'}
    }
  )
};
