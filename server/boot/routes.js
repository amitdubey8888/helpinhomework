var dsConfig = require('../datasources.json');
var config = require('../config.json');
var multer = require('multer');
var fs = require('fs');
var upload = multer({dest: 'public/uploads/'});
var AWS = require('aws-sdk');
AWS.config.accessKeyId = config.accessKeyId;
AWS.config.secretAccessKey = config.secretAccessKey;

//AWS.config.region = "ap-south-1";

var s3 = new AWS.S3();
var myBucket = 'helpinhomeworkdev';
var async = require('async');

module.exports = function (app) {

  var User = app.models.user;
  var Question = app.models.question;

  app.post('/upload-multiple', upload.array('file'), function (req, res, next) {
    console.log('files', req.files);
    console.log('req.body', req.body);
    var directory = req.body.directory || 'public';

    async.eachOf(req.files,
      function (file, index, cb) {
        fs.readFile(file.path, function (err, fileData) {
          if (err) {
            return cb({
              status: 'Failed',
              error: err
            });
          }
          s3.createBucket({Bucket: myBucket}, function (err, data) {
            if (err) {
              cb({
                status: 'Failed',
                error: err
              });
              console.log('Error creating bucket', err);
            }
            else {
              params = {
                Bucket: myBucket,
                ACL: "public-read",
                ContentType: file.mimetype,
                Key: directory + '/' + file.filename + '.' + file.originalname.split('.').reverse()[0],
                Body: fileData
              };
              //noinspection JSUnresolvedFunction
              s3.putObject(params, function (err, data) {
                if (err) {
                  cb({
                    status: 'Failed',
                    error: err
                  });
                  console.log('Error in put object', err)
                } else {
                  console.log('Successfully uploaded data to ' + myBucket + '/' + file.filename, data);
                  cb();
                }
              });
            }
          });
        });
      },
      function (cb) {
        console.log('Final cb', cb);
        if (cb) {
          res.send(cb)
        }
        else {
          req.files.forEach(function (file) {
            file.fileURI = 'https://s3.amazonaws.com/' + myBucket + '/' + directory + '/' + file.filename + '.' + file.originalname.split(".").reverse()[0]
            fs.unlink("public/uploads/" + file.filename);
          })
          res.send({
            status: 'success',
            files: req.files
          });
        }
      });
  });

  app.post('/uploads', upload.single('file'), function (req, res, next) {
    console.log('file', req.file);
    // var directory = 'public/uploads/' + req.file.filename;
    fs.readFile(req.file.path, 'utf8', function (err, data) {
      if (err) {
        res.send({
          status: 'Failed',
          error: err
        });
        return console.log('unable to read file', err);
      }
      awsUpload(data, req.file.filename, res);
    });
  });

  function awsUpload(file, name, res) {
    //noinspection JSUnresolvedFunction
    s3.createBucket({Bucket: myBucket}, function (err, data) {
      if (err) {
        res.send({
          status: 'Failed',
          error: err
        });
        console.log(err);
      } else {
        params = {Bucket: myBucket, Key: name, Body: file};
        //noinspection JSUnresolvedFunction
        s3.putObject(params, function (err, data) {
          if (err) {
            res.send({
              status: 'Failed',
              error: err
            });
            console.log(err)
          } else {
            res.send({
              status: 'Success',
              path: name
            });
            console.log('Successfully uploaded data to ' + myBucket + '/' + name, data);
          }
        });
      }
    });
  }

  var dive = function (dir, action) {
    // Assert that it's a function
    if (typeof action !== "function")
      action = function (error, file) {
        console.log("directory is ", file);
        awsUpload(file);
      };

    // Read the directory
    fs.readdir(dir, function (err, list) {
      // Return the error if something went wrong
      if (err) {
        console.log('readdirerr', err);
        return action(err);
      }

      // For every file in the list
      list.forEach(function (file) {
        // Full path of that file
        var path = dir + "/" + file;
        // Get the file's stats
        fs.stat(path, function (err, stat) {
          // console.log(stat);
          // If the file is a directory
          if (stat && stat.isDirectory())
          // Dive into the directory
            dive(path, action);
          else
          // Call the action
            action(null, path);
        });
      });
    });
  };

  //verified
  app.get('/verified', function (req, res) {
    res.render('verified');
  });

  //send an email with instructions to reset an existing user's password
  app.post('/request-password-reset', function (req, res, next) {
    User.resetPassword({
      email: req.body.email
    }, function (err) {
      if (err) return res.status(401).send(err);

      res.render('response', {
        title: 'Password reset requested',
        content: 'Check your email for further instructions',
        redirectTo: '/',
        redirectToLinkText: 'Log in'
      });
    });
  });

  // show password reset form
  app.get('/reset-password', function (req, res, next) {
    if (!req.accessToken) return res.sendStatus(401);
    res.render('password-reset', {
      accessToken: req.accessToken.id
    });
  });

  //reset the user's password
  app.post('/reset-password', function (req, res, next) {
    if (!req.accessToken) return res.sendStatus(401);

    //verify passwords match
    if (!req.body.password || !req.body.confirmation ||
      req.body.password !== req.body.confirmation) {
      return res.sendStatus(400, new Error('Passwords do not match'));
    }

    User.findById(req.accessToken.userId, function (err, user) {
      if (err) return res.sendStatus(404);
      user.updateAttribute('password', req.body.password, function (err, user) {
        if (err) return res.sendStatus(404);
        console.log('> password reset processed successfully');
        res.render('response', {
          title: 'Password reset success',
          content: 'Your password has been reset successfully',
          redirectTo: '/',
          redirectToLinkText: 'Log in'
        });
      });
    });
  });

  app.get('/profile/:username', function (req, res, next) {
    User.findOne({where: {username: req.params.username}}, function (err, account) {
      if (err) return res.sendStatus(404);
      res.send(account)
    });
  });

  app.get('/profile/:userId/username', function (req, res, next) {
    User.findById(req.params.userId, function (err, user) {
      if (err) return res.sendStatus(404);
      if (user)
        res.send(user.username);
      else
        res.sendStatus(404);
    });
  });

  app.get('/profile/:userId/profile_picture', function (req, res, next) {
    User.findById(req.params.userId, function (err, user) {
      if (err) return res.sendStatus(404);
      if (user)
        res.send(user.profile_picture);
      else
        res.sendStatus(404);
    });
  });

  app.get('/discuss/:questionId', function (req, res, next) {
    Question.findById(req.params.questionId, function (err, question) {
      if (err) return res.sendStatus(404);
      res.send(question)
    })
  })

};
